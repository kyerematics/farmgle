@extends('layouts.main')
@section('body-class')


@endsection


@section('main-content')

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 inner-page-banner wow fadeIn">
        <div class="container">
            <h4>Services</h4>
            <ul class="breadcrumbs">
                <li><a href="{{route('timeline')}}">Home</a></li>
                <li>/</li>
                <li class="active">Services</li>
            </ul>
        </div>
    </div>

    @if($allServices->count() == 0 )
    <div class="section bottom-grass">
        <div class="container">
            <div class="row blog-wrapper blog-grid">
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="{{asset('images/blog_09.jpg')}}" alt="" class="img-responsive  ">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Martines</a></small>
                            <h4><a href="single.html">Curabitur scelerisque risus eget</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat. Nam vitae imperdiet turpis.</p>
                        <div>
                            <a class="readmore" href="single.html">Read More</a><a class="readmore pull-right" href="single.html">Make Appointment</a>

                        </div>
                         </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-3 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="{{asset('images/blog_08.jpg')}}" alt="" class="img-responsive  ">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Semanta</a></small>
                            <h4><a href="single.html">Nullam auctor mi sit amet justo</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Nunc augue purus, posuere in accumsan sodales ac, euismod at est. Nunc faccumsan ermentum consectetur metus placerat mattis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-3 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="{{asset('images/blog_07.jpg')}}" alt="" class="img-responsive  ">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Bob</a></small>
                            <h4><a href="single.html">Fusce eu mi eget mauris eleifend.</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Nam vitae imperdiet turpis. Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="clear"></div>
                <div class="col-md-4 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="{{asset('images/blog_06.jpg')}}" alt="" class="img-responsive  ">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Martines</a></small>
                            <h4><a href="single.html">In nec mi semper, consectetur</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat. Nam vitae imperdiet turpis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-4 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="{{asset('images/blog_05.jpg')}}" alt="" class="img-responsive  ">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Semanta</a></small>
                            <h4><a href="single.html">Sed hendrerit felis at sapien imperdie</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Nunc augue purus, posuere in accumsan sodales ac, euismod at est. Nunc faccumsan ermentum consectetur metus placerat mattis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-4 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="{{asset('images/blog_04.jpg')}}" alt="" class="img-responsive  ">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Bob</a></small>
                            <h4><a href="single.html">Top quality materials gardener</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Nam vitae imperdiet turpis. Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="clear"></div>
                <div class="col-md-3 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="{{asset('images/blog_01.jpg')}}" alt="" class="img-responsive  ">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Martines</a></small>
                            <h4><a href="single.html">Maecenas laoreet metus at</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat. Nam vitae imperdiet turpis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-3 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="{{asset('images/blog_02.jpg')}}" alt="" class="img-responsive  ">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Semanta</a></small>
                            <h4><a href="single.html">The most authentic garden supplies</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Nunc augue purus, posuere in accumsan sodales ac, euismod at est. Nunc faccumsan ermentum consectetur metus placerat mattis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="{{asset('images/blog_03.jpg')}}" alt="" class="img-responsive  ">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Bob</a></small>
                            <h4><a href="single.html">Top quality materials gardener</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Nam vitae imperdiet turpis. Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
            <div class="clearfix"></div>
            <div class="pagination-wrap text-center">
                <ul class="pagination clearfix">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">...</a></li>
                </ul>
            </div>
        </div>
        <!-- end container -->
    </div>
    <!-- end section -->
    @else
        <div class="section bottom-grass">
            <div class="container">
                <div class="row blog-wrapper blog-grid">
                    {{--@foreach($freelancers as $freelancer)
                    <div class="col-md-6 wow fadeIn">
                        <div class="about-widget">
                            <div class="post-media entry">
                                <img src="images/blog_09.jpg" alt="" class="img-responsive  ">
                                <div class="magnifier">
                                    <div class="buttons">
                                        <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                    </div>
                                    <!-- end buttons -->
                                </div>
                                <!-- end magnifier -->
                            </div>
                            <!-- end media -->
                            <div class="small-title">
                                <small>21/12/2016</small>
                                <small><a href="#">by Martines</a></small>
                                <h4><a href="single.html">Curabitur scelerisque risus eget</a></h4>
                            </div>
                            <!-- end small-title -->
                            <p>Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat. Nam vitae imperdiet turpis.</p>
                            <div>
                                <a class="readmore" href="single.html">Read More</a><a class="readmore pull-right" href="single.html">Make Appointment</a>

                            </div>
                        </div>
                        <!-- end about-widget -->
                    </div>
                    <!-- end col -->
                    @endforeach
                    <div class="col-md-3 wow fadeIn">
                        <div class="about-widget">
                            <div class="post-media entry">
                                <img src="images/blog_08.jpg" alt="" class="img-responsive  ">
                                <div class="magnifier">
                                    <div class="buttons">
                                        <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                    </div>
                                    <!-- end buttons -->
                                </div>
                                <!-- end magnifier -->
                            </div>
                            <!-- end media -->
                            <div class="small-title">
                                <small>21/12/2016</small>
                                <small><a href="#">by Semanta</a></small>
                                <h4><a href="single.html">Nullam auctor mi sit amet justo</a></h4>
                            </div>
                            <!-- end small-title -->
                            <p>Nunc augue purus, posuere in accumsan sodales ac, euismod at est. Nunc faccumsan ermentum consectetur metus placerat mattis.</p>
                            <a class="readmore" href="single.html">Read More</a>
                        </div>
                        <!-- end about-widget -->
                    </div>
                    <!-- end col -->
                    <div class="col-md-3 wow fadeIn">
                        <div class="about-widget">
                            <div class="post-media entry">
                                <img src="images/blog_07.jpg" alt="" class="img-responsive  ">
                                <div class="magnifier">
                                    <div class="buttons">
                                        <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                    </div>
                                    <!-- end buttons -->
                                </div>
                                <!-- end magnifier -->
                            </div>
                            <!-- end media -->
                            <div class="small-title">
                                <small>21/12/2016</small>
                                <small><a href="#">by Bob</a></small>
                                <h4><a href="single.html">Fusce eu mi eget mauris eleifend.</a></h4>
                            </div>
                            <!-- end small-title -->
                            <p>Nam vitae imperdiet turpis. Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat.</p>
                            <a class="readmore" href="single.html">Read More</a>
                        </div>
                        <!-- end about-widget -->
                    </div>
                    <!-- end col -->
<div class="clear"></div>--}}

                    @foreach($allServices->chunk(3) as $chunk)
                        @foreach($chunk as $service)
                        @if(\Illuminate\Support\Arr::has($service,'name')){{--If true return institution object--}}
                            <div class="col-md-4 wow fadeIn">
                                <div class="about-widget">
                                    <div class="post-media entry">
                                        <img src="{{$service->getFirstMediaUrl('listing','thumb-577')}}" alt="" class="img-responsive  ">
                                        <div class="magnifier">
                                            <div class="buttons">
                                                <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                            </div>
                                            <!-- end buttons -->
                                        </div>
                                        <!-- end magnifier -->
                                    </div>
                                    <!-- end media -->
                                    <div class="small-title">
                                        <small>Institution</small>
                                        @if(auth()->user()->isFollowing($service->identity->identifier) == true)

                                            <small class="pull-right"><button type="submit" onclick="event.preventDefault();
                                                        document.getElementById('follow-form-{{$service->identity->identifier}}').submit();" class="btn-info">Following{{--Drop down button for user to hit the unfollow route--}}</button>
                                            </small>
                                            <form id="follow-form-{{$service->identity->identifier}}" action="{{ route('unfollow',[$service->identity->identifier]) }}" method="POST" style="display: none;">
                                                @csrf @method('delete')
                                            </form>
                                        @else
                                            <small class="pull-right"><button type="submit" onclick="event.preventDefault();
                                                        document.getElementById('follow-form-{{$service->identity->identifier}}').submit();" class="btn-info">Follow</button>
                                            </small>
                                            <form id="follow-form-{{$service->identity->identifier}}" action="{{ route('follow',[$service->identity->identifier]) }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        @endif
                                    </div>
                                    <!-- end small-title -->
                                    <p>{{str_limit($service->profile->who_you_are,210)}}</p>
                                    <ul class="clearfix">
                                        <li class="text-left"><a href="{{route('services',$service->identity->identifier)}}">View Profile</a></li>
                                        <li class="text-right"><a href="{{route('services',$service->identity->identifier)}}#appointment">Make Appointment</a></li>

                                    </ul>

                                </div>
                                <!-- end about-widget -->
                            </div>

                        @else{{--return Freelancers--}}
                            <div class="col-md-4 wow fadeIn">
                                <div class="about-widget">
                                    <div class="post-media entry">
                                        <img src="{{$service->getFirstMediaUrl('listing','thumb-557')}}" alt="" class="img-responsive  ">
                                        <div class="magnifier">
                                            <div class="buttons">
                                                <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                            </div>
                                            <!-- end buttons -->
                                        </div>
                                        <!-- end magnifier -->
                                    </div>
                                    <!-- end media -->
                                    <div class="small-title">
                                        <small>Freelancer</small>

                                        @if(auth()->user()->isFollowing($service->identity->identifier) == true)

                                            <small class="pull-right"><button type="submit" onclick="event.preventDefault();
                                                        document.getElementById('follow-form-{{$service->identity->identifier}}').submit();" class="btn-info">Following{{--Drop down button for user to hit the unfollow route--}}</button>
                                            </small>
                                            <form id="follow-form-{{$service->identity->identifier}}" action="{{ route('unfollow',[$service->identity->identifier]) }}" method="POST" style="display: none;">
                                                @csrf @method('delete')
                                            </form>
                                        @else
                                        <small class="pull-right"><button type="submit" onclick="event.preventDefault();
                                                     document.getElementById('follow-form-{{$service->identity->identifier}}').submit();" class="btn-info">Follow</button>
                                        </small>
                                            <form id="follow-form-{{$service->identity->identifier}}" action="{{ route('follow',[$service->identity->identifier]) }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        @endif
                                        <h4><a href="{{route('services',$service->identity->identifier)}}">{{$service->profession}}</a></h4>
                                        <h5><a href="{{route('services',$service->identity->identifier)}}">{{$service->user->fullName}}</a></h5>
                                    </div>
                                    <!-- end small-title -->
                                    <p>{{Illuminate\Support\Str::limit($service->profile->who_you_are ?? '',210)}}</p>
                                    <ul class="clearfix">
                                        <li class="text-left"><a href="{{route('services',$service->identity->identifier)}}">View Profile</a></li>
                                        <li class="text-right"><a href="{{route('services',$service->identity->identifier)}}#appointment">Make Appointment</a></li>

                                    </ul>

                                </div>
                                <!-- end about-widget -->
                            </div>
                        @endif
                        @endforeach
                            <div class="clear"></div>
                    @endforeach

                   {{-- <div class="col-md-4 wow fadeIn">
                        <div class="about-widget">
                            <div class="post-media entry">
                                <img src="images/blog_04.jpg" alt="" class="img-responsive  ">
                                <div class="magnifier">
                                    <div class="buttons">
                                        <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                    </div>
                                    <!-- end buttons -->
                                </div>
                                <!-- end magnifier -->
                            </div>
                            <!-- end media -->
                            <div class="small-title">
                                <small>21/12/2016</small>
                                <small><a href="#">by Bob</a></small>
                                <h4><a href="single.html">Top quality materials gardener</a></h4>
                            </div>
                            <!-- end small-title -->
                            <p>Nam vitae imperdiet turpis. Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat.</p>
                            <a class="readmore" href="single.html">Read More</a>
                        </div>
                        <!-- end about-widget -->
                    </div>
                    <!-- end col -->
                    <div class="clear"></div>
                    <div class="col-md-3 wow fadeIn">
                        <div class="about-widget">
                            <div class="post-media entry">
                                <img src="images/blog_01.jpg" alt="" class="img-responsive  ">
                                <div class="magnifier">
                                    <div class="buttons">
                                        <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                    </div>
                                    <!-- end buttons -->
                                </div>
                                <!-- end magnifier -->
                            </div>
                            <!-- end media -->
                            <div class="small-title">
                                <small>21/12/2016</small>
                                <small><a href="#">by Martines</a></small>
                                <h4><a href="single.html">Maecenas laoreet metus at</a></h4>
                            </div>
                            <!-- end small-title -->
                            <p>Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat. Nam vitae imperdiet turpis.</p>
                            <a class="readmore" href="single.html">Read More</a>
                        </div>
                        <!-- end about-widget -->
                    </div>
                    <!-- end col -->
                    <div class="col-md-3 wow fadeIn">
                        <div class="about-widget">
                            <div class="post-media entry">
                                <img src="images/blog_02.jpg" alt="" class="img-responsive  ">
                                <div class="magnifier">
                                    <div class="buttons">
                                        <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                    </div>
                                    <!-- end buttons -->
                                </div>
                                <!-- end magnifier -->
                            </div>
                            <!-- end media -->
                            <div class="small-title">
                                <small>21/12/2016</small>
                                <small><a href="#">by Semanta</a></small>
                                <h4><a href="single.html">The most authentic garden supplies</a></h4>
                            </div>
                            <!-- end small-title -->
                            <p>Nunc augue purus, posuere in accumsan sodales ac, euismod at est. Nunc faccumsan ermentum consectetur metus placerat mattis.</p>
                            <a class="readmore" href="single.html">Read More</a>
                        </div>
                        <!-- end about-widget -->
                    </div>
                    <!-- end col -->
                    <div class="col-md-6 wow fadeIn">
                        <div class="about-widget">
                            <div class="post-media entry">
                                <img src="images/blog_03.jpg" alt="" class="img-responsive  ">
                                <div class="magnifier">
                                    <div class="buttons">
                                        <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                    </div>
                                    <!-- end buttons -->
                                </div>
                                <!-- end magnifier -->
                            </div>
                            <!-- end media -->
                            <div class="small-title">
                                <small>21/12/2016</small>
                                <small><a href="#">by Bob</a></small>
                                <h4><a href="single.html">Top quality materials gardener</a></h4>
                            </div>
                            <!-- end small-title -->
                            <p>Nam vitae imperdiet turpis. Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat.</p>
                            <a class="readmore" href="single.html">Read More</a>
                        </div>
                        <!-- end about-widget -->
                    </div>
                    <!-- end col -->--}}
                </div>
                <!-- end row -->
                <div class="clearfix"></div>
                <div class="pagination-wrap text-center">
                    <ul class="pagination clearfix">
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">...</a></li>
                    </ul>
                </div>
            </div>
            <!-- end container -->
        </div>
        <!-- end section -->
    @endif

@endsection

@section('custom-script')


@endsection
