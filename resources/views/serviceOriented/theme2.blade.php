@extends('layouts.main')

@section('body-class')
services-page
@endsection

@section('main-content')
    <div class="section lb information-section wow fadeIn">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="imformation-text">
                            <h2>WELCOME TO Grasscare garden</h2>
                            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non
                                numquam eius modi tempora incidunt ut labore et dolore magnam aliq uam quae rat voluptatem.
                                Ut enim ad minima veniam, quis nostrum.
                            </p>
                            <h4 class="inform-service">We are available for 24/7 for Moving Services</h4>
                            <ul>
                                <li><i class="fa fa-angle-right"></i> LAWN RENOVATION</li>
                                <li><i class="fa fa-angle-right"></i> COMPLETE DEVELOPMENT</li>
                                <li><i class="fa fa-angle-right"></i> TREE & SHRUB CARE</li>
                                <li><i class="fa fa-angle-right"></i> PETS CONTROL IN YOUR LAWN</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="imformation-img">
                            <img src="images/house-img.png" alt="#" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="about-us wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <div class="img-border">
                        <img src="images/team_02.jpg" alt="Image">
                    </div>
                </div>
                <div class="col-lg-5 col-md-8 col-sm-12 col-xs-12">
                    <h2>We are <b>Grasscare Group</b><br>DESIGN AND CONSTRUCTION</h2>
                    <p class="lead">A Professional Gardening Service for Ever!</p>
                    <p>Lorem ipsum dolor sit amet, cons ectetur elit. Vestibulum nec odios Suspe ndisse cursus mal suada faci lisis. Lorem ipsum dolor sit ametion consectetur elit. Vesti bulum nec odio ipsum.</p>
                    <ul>
                        <li><i class="fa fa-angle-right"></i> Lawn Renovation<br></li>
                        <li><i class="fa fa-angle-right"></i> Landscape Lighting<br></li>
                        <li><i class="fa fa-angle-right"></i> Flowerscaping<br></li>
                        <li><i class="fa fa-angle-right"></i> Natural Stone Walks</li>
                    </ul>
                    <ul>
                        <li><i class="fa fa-angle-right"></i> Lawn Maintenance<br></li>
                        <li><i class="fa fa-angle-right"></i> Seasonal Clean-ups<br></li>
                        <li><i class="fa fa-angle-right"></i> Snow Plowing &amp; Salting<br></li>
                        <li><i class="fa fa-angle-right"></i> Tree &amp; Shrub Care</li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col-lg-offset-0 col-sm-offset-0 col-xs-offset-0 lawn-maintenance">
                    <h3>Lawn <b>maintenance</b></h3>
                    <p>Lorem ipsum dolor sit amet, cons ectetur elit. Vestibulum nec odios Suspe ndisse cursus mal suada faci lisis. Lorem ipsum dolor sit ametion consectetur. </p>
                    <img class="img-responsive" src="images/page_parallax_01.jpg" alt="Image">
                </div>
            </div>
        </div>
    </section>
    <!-- end add new section -->
    <section class="gray-section-info white-color wow fadeIn">
        <div class="icon-main"><img src="images/main-icon.png" alt="#"></div>
        <h2 class="center-text"><b>WHY CHOOSE US</b></h2>
        <p class="lead center-text">Praesent porta nulla at arcu ultricies, at fermentum ipsum rhoncus praesent. Nulla tincidunt nisl id tincidunt aliquam eros.</p>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="aio-icon-component    style_1">
                    <div id="Info-box-wrap-2985" class="aio-icon-box left-icon" style="">
                        <div class="aio-icon-left">
                            <div class="ult-just-icon-wrapper  ">
                                <div class="align-icon" style="text-align:center;">
                                    <div class="aio-icon"><img src="images/cutter.png" alt="#" /></div>
                                </div>
                            </div>
                        </div>
                        <div class="aio-ibd-block">
                            <div class="aio-icon-header">
                                <h3 class="aio-icon-title ult-responsive" style="font-weight:700;color:#ffffff;">Hedge Cutting</h3>
                            </div>
                            <div class="aio-icon-description ult-responsive" style="font-weight:300;color:#ebebeb;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc turpis ligula, consequat sit amet placerat vitae, condimentum ut est.</div>
                            <!-- description -->
                        </div>
                        <!-- aio-ibd-block -->
                    </div>
                    <!-- aio-icon-box -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="aio-icon-component    style_1">
                    <div id="Info-box-wrap-2986" class="aio-icon-box left-icon" style="">
                        <div class="aio-icon-left">
                            <div class="ult-just-icon-wrapper  ">
                                <div class="align-icon" style="text-align:center;">
                                    <div class="aio-icon"><img src="images/clean.png" alt="#" /></div>
                                </div>
                            </div>
                        </div>
                        <div class="aio-ibd-block">
                            <div class="aio-icon-header">
                                <h3 class="aio-icon-title ult-responsive" style="font-weight:700;color:#ffffff;">Garden Clean Up</h3>
                            </div>
                            <div class="aio-icon-description ult-responsive" style="font-weight:300;color:#ebebeb;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc turpis ligula, consequat sit amet placerat vitae, condimentum ut est.</div>
                            <!-- description -->
                        </div>
                        <!-- aio-ibd-block -->
                    </div>
                    <!-- aio-icon-box -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="aio-icon-component    style_1">
                    <div id="Info-box-wrap-2987" class="aio-icon-box left-icon" style="">
                        <div class="aio-icon-left">
                            <div class="ult-just-icon-wrapper  ">
                                <div class="align-icon" style="text-align:center;">
                                    <div class="aio-icon"><img src="images/landcap.png" alt="#" /></div>
                                </div>
                            </div>
                        </div>
                        <div class="aio-ibd-block">
                            <div class="aio-icon-header">
                                <h3 class="aio-icon-title ult-responsive" style="font-weight:700;color:#ffffff;">Landscape</h3>
                            </div>
                            <div class="aio-icon-description ult-responsive" style="font-weight:300;color:#ebebeb;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc turpis ligula, consequat sit amet placerat vitae, condimentum ut est.</div>
                            <!-- description -->
                        </div>
                        <!-- aio-ibd-block -->
                    </div>
                    <!-- aio-icon-box -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="aio-icon-component    style_1">
                    <div id="Info-box-wrap-2988" class="aio-icon-box left-icon" style="">
                        <div class="aio-icon-left">
                            <div class="ult-just-icon-wrapper  ">
                                <div class="align-icon" style="text-align:center;">
                                    <div class="aio-icon"><img src="images/walter-s.png" alt="#" /></div>
                                </div>
                            </div>
                        </div>
                        <div class="aio-ibd-block">
                            <div class="aio-icon-header">
                                <h3 class="aio-icon-title ult-responsive" style="font-weight:700;color:#ffffff;">Watering & Irrigation</h3>
                            </div>
                            <div class="aio-icon-description ult-responsive" style="font-weight:300;color:#ebebeb;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc turpis ligula, consequat sit amet placerat vitae, condimentum ut est.</div>
                            <!-- description -->
                        </div>
                        <!-- aio-ibd-block -->
                    </div>
                    <!-- aio-icon-box -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="aio-icon-component    style_1">
                    <div id="Info-box-wrap-2989" class="aio-icon-box left-icon" style="">
                        <div class="aio-icon-left">
                            <div class="ult-just-icon-wrapper  ">
                                <div class="align-icon" style="text-align:center;">
                                    <div class="aio-icon"><img src="images/tree-sr.png" alt="#" /></div>
                                </div>
                            </div>
                        </div>
                        <div class="aio-ibd-block">
                            <div class="aio-icon-header">
                                <h3 class="aio-icon-title ult-responsive" style="font-weight:700;color:#ffffff;">Tree Surgery</h3>
                            </div>
                            <div class="aio-icon-description ult-responsive" style="font-weight:300;color:#ebebeb;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc turpis ligula, consequat sit amet placerat vitae, condimentum ut est.</div>
                            <!-- description -->
                        </div>
                        <!-- aio-ibd-block -->
                    </div>
                    <!-- aio-icon-box -->
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="aio-icon-component style_1">
                    <div id="Info-box-wrap-2990" class="aio-icon-box left-icon" style="">
                        <div class="aio-icon-left">
                            <div class="ult-just-icon-wrapper  ">
                                <div class="align-icon" style="text-align:center;">
                                    <div class="aio-icon"><img src="images/fruit.png" alt="#" /></div>
                                </div>
                            </div>
                        </div>
                        <div class="aio-ibd-block">
                            <div class="aio-icon-header">
                                <h3 class="aio-icon-title ult-responsive" style="font-weight:700;color:#ffffff;">Fruits</h3>
                            </div>
                            <div class="aio-icon-description ult-responsive" style="font-weight:300;color:#ebebeb;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc turpis ligula, consequat sit amet placerat vitae, condimentum ut est.</div>
                            <!-- description -->
                        </div>
                        <!-- aio-ibd-block -->
                    </div>
                    <!-- aio-icon-box -->
                </div>
            </div>
        </div>
    </section>
    <section class="our-mission wow fadeIn">
        <div class="container">
            <div class="mission-img"><img src="images/our-mission.png" alt="#" ></div>
            <div class="mission-text">
                <h2 class="center-text"><b>OUR MISSION</b></h2>
                <p>Sed placerat molestie varius. Fusce a sagittis tortor, vitae maximus nulla. Class aptent taciti sociosqu ad litora
                    torquent per conubia nostra, per inceptos himenaeos. Donec porta nisl eu ante facilisis convallis. Mauris sed porttitor lectus, eget vulputate dui.
                    Nulla cursus semper nisl, vel ultricies velit pulvinar non.
                </p>
            </div>
        </div>
    </section>

    <div class="section lb our-gar wow fadeIn">
        <div class="container">
            <div class="section-title">
                <i class="flaticon-people"></i>
                <h3>Our Gardeners</h3>
                <p class="lead">A Professional Team of Gardeners</p>
            </div>
            <!-- end title -->
            <div class="row team-members">
                <div class="col-md-4 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/team_05.jpg" alt="" class="img-responsive" />
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-facebook"></span></a>
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-twitter"></span></a>
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-google-plus"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>CEO / Founder</small>
                            <h4>Martin Martines</h4>
                        </div>
                        <!-- end small-title -->
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-4 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/team_06.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-facebook"></span></a>
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-twitter"></span></a>
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-google-plus"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>CEO / Founder</small>
                            <h4>Linda Martines</h4>
                        </div>
                        <!-- end small-title -->
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-4 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/team_07.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-facebook"></span></a>
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-twitter"></span></a>
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-google-plus"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>Pro Gardener</small>
                            <h4>Semanta Doe</h4>
                        </div>
                        <!-- end small-title -->
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end section -->
    <!-- appointment section -->
    <section class="appointment-section wow fadeIn">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 lef-appoint-img">
                    <div class="row">
                        <img src="images/appoimnet-img.jpg" alt="#" />
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 appointment">
                    <div class="row">
                        <div class="col-md-12">
                            <h6 style="display:none;">Appointment</h6>
                            <form method="post">
                                <fieldset class="row appointment_form">
                                    <div class="col-md-6">
                                        <label class="sr-only">Appointment time</label>
                                        <input type="text" placeholder="MM/DD/YYYY" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="sr-only">Your name</label>
                                        <input type="text" placeholder="Your full name *" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="sr-only">Phone number</label>
                                        <input type="text" placeholder="Phone number *" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="sr-only">Email address</label>
                                        <input type="email" placeholder="Email address *" class="form-control">
                                    </div>
                                    <div class="col-md-6 gardener-select">
                                        <label class="sr-only">Select Gardener</label>
                                        <select class="selectpicker" data-style="btn-white">
                                            <option value="12">-- Select Gardener --</option>
                                            <option value="amanda">Amanda JOE</option>
                                            <option value="mark">Mark DOE</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="sr-only">Select Service</label>
                                        <select class="selectpicker" data-style="btn-white">
                                            <option value="12">-- Select Service --</option>
                                            <option value="12">Garden Watering</option>
                                            <option value="13">Preparing Landscape</option>
                                            <option value="15">Garden Fence</option>
                                            <option value="14">Garden Supplies</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <textarea placeholder="Add Extra Notes" class="form-control"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="center"><button type="reset" class="btn btn-primary btn-lg">Send Now</button></div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                </div>
            </div>
        </div>
    </section>
    <!-- appointment section -->
    <!-- end section -->

    <div class="bottom-section wow fadeIn">
        <div class="container">
            <div class="col-lg-7 col-md-7 col-sm-4 col-xs-12"></div>
            <div class="col-lg-5 col-md-5 col-sm-8 col-xs-12">
                <p>When you are looking a gardener for your personal works, just drop a message to us!</p>
                <div class="center"><a class="btn-contact" href="page-contact.html">Contact</a></div>
            </div>
        </div>
    </div>
    <div id="map"></div>
    <!-- end blog -->
    <!-- end section -->
    <div class="section lb blog wow fadeIn">
        <div class="container">
            <div class="section-title">
                <i class="flaticon-link"></i>
                <h3>From the Blog</h3>
                <p class="lead">Learn garden, landscaping with our blog posts</p>
            </div>
            <!-- end title -->
            <div class="row blog-wrapper">
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/blog_01.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Martines</a></small>
                            <h4><a href="single.html">benches made for gardens</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat. Nam vitae imperdiet turpis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/blog_02.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Semanta</a></small>
                            <h4><a href="single.html">The most authentic garden supplies</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Nunc augue purus, posuere in accumsan sodales ac, euismod at est. Nunc faccumsan ermentum consectetur metus placerat mattis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/blog_03.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Bob</a></small>
                            <h4><a href="single.html">Top quality materials gardener</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Nam vitae imperdiet turpis. Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/blog_01.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Martines</a></small>
                            <h4><a href="single.html">benches made for gardens</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat. Nam vitae imperdiet turpis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end blog -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end section -->

@endsection

@section('custom-script')
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    <!-- MAP & CONTACT -->
    <script src="{{asset('js/map.js')}}"></script>
@endsection
