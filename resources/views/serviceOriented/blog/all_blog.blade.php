@extends('layouts.main')
@section('body-class')


@endsection


@section('main-content')
    <div class="section lb blog wow fadeIn">
        <div class="container">
            <div class="section-title">
                <i class="flaticon-link"></i>
                <h3>From the Blog</h3>
                <p class="lead">Learn garden, landscaping with our blog posts</p>
            </div>
            <!-- end title -->
            <div class="row blog-wrapper">
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/blog_01.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Martines</a></small>
                            <h4><a href="single.html">benches made for gardens</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat. Nam vitae imperdiet turpis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/blog_02.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Semanta</a></small>
                            <h4><a href="single.html">The most authentic garden supplies</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Nunc augue purus, posuere in accumsan sodales ac, euismod at est. Nunc faccumsan ermentum consectetur metus placerat mattis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/blog_03.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Bob</a></small>
                            <h4><a href="single.html">Top quality materials gardener</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Nam vitae imperdiet turpis. Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/blog_01.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Martines</a></small>
                            <h4><a href="single.html">benches made for gardens</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat. Nam vitae imperdiet turpis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end blog -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>

@endsection

@section('custom-script')


@endsection
