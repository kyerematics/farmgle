@extends('layouts.main')

@section('body-class')
Single-blog-page
@endsection

@section('main-content')

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 inner-page-banner wow fadeIn">
        <div class="container">
            <h4>Single Blog</h4>
            <ul class="breadcrumbs">
                <li><a href="{{route('timeline')}}">Home</a></li>
                <li>/</li>
                <li class="active">Single Blog</li>
            </ul>
        </div>
    </div>
    <!-- end section -->
    <div class="section single-blog bottom-grass wow fadeIn">
        <div class="container">
            <!-- blog contact -->
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="feature-img-post">
                        <img src="{{asset('images/post-img.jpg')}}" class="img-responsive" alt="#" />
                        <div class="post-date">
                            <span>05 / Nov / 2017</span>
                        </div>
                    </div>
                    <div class="feature-blog-heading">
                        <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</h2>
                    </div>
                    <div class="post-resource">
                        <ul>
                            <li><i class="fa fa-user"></i><a href="#">Gardencare</a></li>
                            <li><i class="fa fa-tag"></i><a href="#" rel="category tag">Outside</a></li>
                            <li><i class="fa fa-comments"></i><a href="#">5 comments</a></li>
                        </ul>
                    </div>
                    <p class="single-blog">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                        totam rem ape riam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui
                        ratione voluptatem sequi nesciunt nam aliquam quaerat voluptatem. Ut enim ad inima veniam, quis nostrum exercitationem ullam
                        corporis suscipit laboriosam, nisi ut aliquid ex ea commodi.
                    </p>
                    <h4>Eaque ipsa quae ab illo inventore veritatis et quase</h4>
                    <p class="single-blog">Architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,
                        sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                    </p>
                    <ul class="blog-list">
                        <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Dolor sit amet, consectetur</li>
                        <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Sdipiscing elit,</li>
                        <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Seddo eiusmod tempor</li>
                        <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Incididunt ut labore et</li>
                        <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Eolore magna aliqua.</li>
                        <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Dolor sit amet, consectetur</li>
                        <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Sdipiscing elit,</li>
                        <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Seddo eiusmod tempor</li>
                        <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Incididunt ut labore et</li>
                        <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Eolore magna aliqua.</li>
                    </ul>
                    <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                    <p class="single-blog">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit
                        in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                    </p>
                </div>
                <div class="row">
                    <div class="clients-say">
                        <p><i class="fa fa-quote-left"></i> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                            totam rem ape riam, eaque ipsa quae ab illo invent ore veritatis et quasi architecto beatae vitae.
                            <span class="text-right"> -John Michale</span>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="share-post">
                            <a href="#" class="share-text">Share</a>
                            <ul class="social-icon">
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="commant-section">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>COMMENTS: (5)</h3>
                        <div class="commant-text row">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-hidden">
                                <div class="profile">
                                    <img class="img-responsive" src="{{asset('images/client1.jpg')}}" alt="#" />
                                </div>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-hidden">
                                <h5>David</h5>
                                <p><span class="c_date">March 2, 2016</span> | <span><a rel="nofollow" class="comment-reply-link" href="#" >Reply</a></span></p>
                                <p class="msg">ThisThis book is a treatise on the theory of ethics, very popular during the Renaissance.
                                    The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet..
                                </p>
                            </div>
                        </div>
                        <div class="commant-text row">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-hidden">
                                <div class="profile">
                                    <img class="img-responsive" src="{{asset('images/client2.jpg')}}" alt="#" />
                                </div>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-hidden">
                                <h5>Jack</h5>
                                <p><span class="c_date">March 2, 2016</span> | <span><a rel="nofollow" class="comment-reply-link" href="#" >Reply</a></span></p>
                                <p class="msg">Lorem ipsum dolor sit amet, consect etur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ali qua. Ut enim.</p>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1"></div>
                            <div class="col-lg-11 col-md-11 col-sm-11">
                                <div class="commant-text row">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-hidden">
                                        <div class="profile">
                                            <img class="img-responsive" src="{{asset('images/client3.jpg')}}" alt="#" />
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-hidden">
                                        <h5>Romy</h5>
                                        <p><span class="c_date">March 2, 2016</span> | <span><a rel="nofollow" class="comment-reply-link" href="#" >Reply</a></span></p>
                                        <p class="msg">ThisThis book is a treatise on the theory of ethics, very popular during the Renaissance.
                                            The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet..”, comes from a line in section 1.10.32.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1"></div>
                                <div class="col-lg-11 col-md-11 col-sm-11">
                                    <div class="commant-text row">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-hidden">
                                            <div class="profile">
                                                <img class="img-responsive" src="{{asset('images/client4.jpg')}}" alt="#" />
                                            </div>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-hidden">
                                            <h5>Gerhard</h5>
                                            <p><span class="c_date">March 2, 2016</span> | <span><a rel="nofollow" class="comment-reply-link" href="#" >Reply</a></span></p>
                                            <p class="msg">Lorem ipsum dolor sit amet, consect etur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ali qua. Ut enim.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="commant-text row">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-hidden">
                                <div class="profile">
                                    <img class="img-responsive" src="{{asset('images/client4.jpg')}}" alt="#" />
                                </div>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-hidden">
                                <h5>Gerhard</h5>
                                <p><span class="c_date">March 2, 2016</span> | <span><a rel="nofollow" class="comment-reply-link" href="#" >Reply</a></span></p>
                                <p class="msg">ThisThis book is a treatise on the theory of ethics..</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="commant-section">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Leave a comment</h3>
                        <div class="form-command row">
                            <form action="#" method="post" id="comments_form" class="form-horizontal1" >
                                <div class="comment-box-field">

                                    <div class=" col-sm-12 col-xs-12">
                                        <div class="comment-box-full"><textarea id="comments" class="form-control4" placeholder="Comments" name="comment" cols="45" rows="3" aria-required="true"></textarea></div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="comment-box-submit">
                                            <input id="submit" value="Submit " type="submit">
                                        </div>
                                    </div>
                                    <input name="comment_post_ID" value="348" id="comment_post_ID" type="hidden">
                                    <input name="comment_parent" id="comment_parent" value="0" type="hidden">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <aside class="side-bar">
                    <div class="search-box">
                        <div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" class="  search-query form-control" placeholder="Search" />
                                <span class="input-group-btn">
                              <button class="btn btn-danger" type="button">
                              <span class=" glyphicon glyphicon-search"></span>
                              </button>
                              </span>
                            </div>
                        </div>
                    </div>
                    <div class="side-bar-blog">
                        <h3>Popular News & Tips</h3>
                        <ul>
                            <li>
                                <img class="post-thumb" src="{{asset('images/img-cap4.jpg')}}" alt="" />
                                <div class="content-wrap">
                                    <h5>3 Reasons to Build A Backyard Fire Pit</h5>
                                    <span class="date">8 Nov 2017</span>
                                </div>
                            </li>
                            <li>
                                <img class="post-thumb" src="{{asset('images/img-cap5.jpg')}}" alt="" />
                                <div class="content-wrap">
                                    <h5>3 Reasons to Build A Backyard Fire Pit</h5>
                                    <span class="date">8 Nov 2017</span>
                                </div>
                            </li>
                            <li>
                                <img class="post-thumb" src="{{asset('images/img-cap6.jpg')}}" alt="" />
                                <div class="content-wrap">
                                    <h5>3 Reasons to Build A Backyard Fire Pit</h5>
                                    <span class="date">8 Nov 2017</span>
                                </div>
                            </li>
                            <li>
                                <img class="post-thumb" src="{{asset('images/img-cap4.jpg')}}" alt="" />
                                <div class="content-wrap">
                                    <h5>3 Reasons to Build A Backyard Fire Pit</h5>
                                    <span class="date">8 Nov 2017</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="side-bar-blog">
                        <h3>About gardener</h3>
                        <div class="white-blog">
                            <p>Gardener Company provide the design and installation of landscape construction projects including walkways, steps, retaining walls, patios,
                                veneer stone, planting, trans-planting,drainage systems, lawn renovation and installation, paver driveways, cobblestone edging, and excavation.
                            </p>
                        </div>
                    </div>
                    <div class="side-bar-blog catargy">
                        <h3>Categories</h3>
                        <ul class="wht">
                            <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Blog</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Blog 2 Col</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Construction Tips</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Outside</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Uncategorized</a></li>
                        </ul>
                    </div>
                    <div class="side-bar-blog catargy">
                        <h3>TAGS WIDGET</h3>
                        <a class="tag" href="#">Drain Cleaning</a><a class="tag" href="#">Kitchen Plumbing</a><a class="tag" href="#">outide Plumbing</a><a class="tag" href="#">Pipe fixes</a>
                        <a class="tag" href="#">Pipe leakages</a><a class="tag" href="#">tips</a>
                    </div>
                </aside>
            </div>
            <!-- end blog contact -->
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end section -->
    <!-- blog -->
@endsection

@section('custom-script')


@endsection
