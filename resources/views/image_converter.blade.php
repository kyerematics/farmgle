@extends('layouts.main')
@section('body-class')


@endsection


@section('main-content')
<div class="container">
    <div class="row">
        <form class="col-md-12" action="{{route('image.converter')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-field">
                <input type="file" name="images[]" multiple>
            </div>
            <input type="submit" title="Submit">
        </form>
    </div>
</div>

@endsection

@section('custom-script')


@endsection
