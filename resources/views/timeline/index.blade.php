@extends('layouts.main')
@section('body-class')


@endsection


@section('main-content')
    <div class="section bottom-grass">
        <div class="container">

            <div class="row blog-wrapper blog-grid">
                @if($userTimeline->count()==0)

                    <h1>No Post on Timeline</h1>
                @else
                @foreach($userTimeline->chunk(3) as $chunk)
                    @foreach($chunk as $timeline)
                        {{--If true return institution object--}}
                        @if(\Illuminate\Support\Arr::has($timeline,'name'))
                        <div class="col-md-4 wow fadeIn">
                            <div class="about-widget">
                                <div class="post-media entry">
                                    <img src="{{$timeline->getFirstMediaUrl('listing','thumb-577')}}" alt="" class="img-responsive  ">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                        </div>
                                        <!-- end buttons -->
                                    </div>
                                    <!-- end magnifier -->
                                </div>
                                <!-- end media -->
                                <div class="small-title">
                                    <small>Institution</small>
                                    @if(auth()->user()->isFollowing($timeline->identity->identifier) == true)

                                        <small class="pull-right"><button type="submit" onclick="event.preventDefault();
                                                    document.getElementById('follow-form-{{$timeline->identity->identifier}}').submit();" class="btn-info">Following{{--Drop down button for user to hit the unfollow route--}}</button>
                                        </small>
                                        <form id="follow-form-{{$timeline->identity->identifier}}" action="{{ route('unfollow',[$timeline->identity->identifier]) }}" method="POST" style="display: none;">
                                            @csrf @method('delete')
                                        </form>
                                    @else
                                        <small class="pull-right"><button type="submit" onclick="event.preventDefault();
                                                    document.getElementById('follow-form-{{$timeline->identity->identifier}}').submit();" class="btn-info">Follow</button>
                                        </small>
                                        <form id="follow-form-{{$timeline->identity->identifier}}" action="{{ route('follow',[$timeline->identity->identifier]) }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    @endif
                                </div>
                                <!-- end small-title -->
                                <p>{{str_limit($timeline->profile->who_you_are,210)}}</p>
                                <ul class="clearfix">
                                    <li class="text-left"><a href="{{route('services',$timeline->identity->identifier)}}">View Profile</a></li>
                                    <li class="text-right"><a href="{{route('services',$timeline->identity->identifier)}}#appointment">Make Appointment</a></li>

                                </ul>

                            </div>
                            <!-- end about-widget -->
                        </div>

                        @elseif(\Illuminate\Support\Arr::has($timeline,'profession'))
                            {{--return Freelancers--}}
                        <div class="col-md-4 wow fadeIn">
                            <div class="about-widget">
                                <div class="post-media entry">
                                    <img src="{{$timeline->getFirstMediaUrl('listing','thumb-557')}}" alt="" class="img-responsive  ">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                        </div>
                                        <!-- end buttons -->
                                    </div>
                                    <!-- end magnifier -->
                                </div>
                                <!-- end media -->
                                <div class="small-title">
                                    <small>Freelancer</small>

                                    @if(auth()->user()->isFollowing($timeline->identity->identifier) == true)

                                        <small class="pull-right"><button type="submit" onclick="event.preventDefault();
                                                    document.getElementById('follow-form-{{$timeline->identity->identifier}}').submit();" class="btn-info">Following{{--Drop down button for user to hit the unfollow route--}}</button>
                                        </small>
                                        <form id="follow-form-{{$timeline->identity->identifier}}" action="{{ route('unfollow',[$timeline->identity->identifier]) }}" method="POST" style="display: none;">
                                            @csrf @method('delete')
                                        </form>
                                    @else
                                        <small class="pull-right"><button type="submit" onclick="event.preventDefault();
                                                    document.getElementById('follow-form-{{$timeline->identity->identifier}}').submit();" class="btn-info">Follow</button>
                                        </small>
                                        <form id="follow-form-{{$timeline->identity->identifier}}" action="{{ route('follow',[$timeline->identity->identifier]) }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    @endif
                                    <h4><a href="{{route('services',$timeline->identity->identifier)}}">{{$timeline->profession}}</a></h4>
                                    <h5><a href="{{route('services',$timeline->identity->identifier)}}">{{$timeline->user->fullName}}</a></h5>
                                </div>
                                <!-- end small-title -->
                                <p>{{Illuminate\Support\Str::limit($timeline->profile->who_you_are ?? '',210)}}</p>
                                <ul class="clearfix">
                                    <li class="text-left"><a href="{{route('services',$timeline->identity->identifier)}}">View Profile</a></li>
                                    <li class="text-right"><a href="{{route('services',$timeline->identity->identifier)}}#appointment">Make Appointment</a></li>

                                </ul>

                            </div>
                            <!-- end about-widget -->
                        </div>

                        @elseif(\Illuminate\Support\Arr::has($timeline,'product'))
                            {{--Return product--}}

                            <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro text-center" style="visibility: visible; animation-name: fadeIn;">
                                <div class="shop-wrapper">
                                    <div class="post-media entry">
                                        <img src="{{$timeline->getFirstMediaUrl('product','grid')}}" alt="" class="img-responsive img-thumbnail">
                                        <div class="magnifier">
                                            <div class="buttons">
                                                <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end media -->
                                    <div class="shop-meta">
                                        <small><a href="{{route('singleProduct',[$timeline->producible->identity->identifier,$timeline->uuid])}}">{{$timeline->getLocalPrice()}}</a></small>
                                        <h4><a href="{{route('singleProduct',[$timeline->producible->identity->identifier,$timeline->uuid])}}">{{$timeline->product}}</a></h4>
                                    </div>
                                    <!-- end shop-meta -->
                                </div>
                                <!-- end shop-wrapper -->
                            </div>
                            <!-- end col -->
                        @elseif(\Illuminate\Support\Arr::has($timeline,'title'))
                            {{--Returns post--}}

                        @else
                        {{--@elseif(\Illuminate\Support\Arr::has($timeline,'mini'))--}}
                            {{--Returns mini-post--}}
                        @endif
                    @endforeach
                    <div class="clear"></div>
                @endforeach
                @endif
            </div>
        @if($userTimeline->count()!==0)
            <!-- end row -->
            <div class="clearfix"></div>
            <div class="pagination-wrap text-center">
                <ul class="pagination clearfix">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">...</a></li>
                </ul>
            </div>
            @endif
        </div>
        <!-- end container -->
    </div>
    <!-- end section -->

@endsection

@section('custom-script')


@endsection
