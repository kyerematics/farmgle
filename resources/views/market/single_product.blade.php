@extends('layouts.main')
@section('body-class')

@endsection


@section('main-content')


    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 inner-page-banner wow fadeIn">
        <div class="container">
            <h4>{{$item->product}}</h4>
            <ul class="breadcrumbs">
                <li><a href="index-2.html">Home</a></li>
                <li>/</li>
                <li><a href="shop.html">Shop</a></li>
                <li>/</li>
                <li class="active">{{$item->product}}</li>
            </ul>
        </div>
    </div>
    <!-- end section -->
    <div class="section product-img-main bottom-grass wow fadeIn">
        <div class="container">
            <!-- blog contact -->
            <div class="col-sm-5">
                <div id="carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="{{$item->getFirstMediaUrl('product','single')}}" alt="#" />
                        </div>

                        @foreach($item->getMedia('product') as $images)
                        <div class="item">
                            <img src="{{$images->getUrl('single')}}" alt="#" />
                        </div>
                        @endforeach

                    </div>
                </div>
                <div class="clearfix">
                    <div id="thumbcarousel" class="carousel slide" data-interval="false">
                        <div class="carousel-inner">
                            <div class="item active">
                                <div data-target="#carousel" data-slide-to="0" class="thumb"><img src="{{$item->getFirstMediaUrl('product','single')}}" alt="#" /></div>
                            </div>
                            <!-- /item -->

                            <div class="item">
                                @foreach($item->getMedia('product') as $key => $media)
                                <div data-target="#carousel" data-slide-to="{{$key+1}}" class="thumb"><img src="{{$media->getUrl('single')}}" alt="#" /></div>
                            @endforeach
                            </div>
                            <!-- /item -->
                        </div>
                        <!-- /carousel-inner -->
                        <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                    <!-- /thumbcarousel -->
                </div>
                <!-- /clearfix -->
            </div>
            <!-- /col-sm-5 -->
            <div class="col-sm-7">
                <div class="product-heading">
                    <h2>{{$item->product}}</h2>
                </div>
                <div class="product-detail-side">
                    <span><del>{{$item->getOldPrice()}}</del></span><span class="new-price">{{$item->getLocalPrice()}}</span>
                    <span class="rating">
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star-o" aria-hidden="true"></i>
                     </span>
                    <span class="review">(5 customer review)</span>
                </div>
                <div class="detail-contant">
                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.
                        Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<br><span class="stock">2 in stock</span>
                    </p>
                    <form class="cart" method="post" action="http://psdconverthtml.com/live/grasscare/shop-cart.html" >
                        <div class="quantity"><input type="number" step="1" min="1" max="5" name="quantity" value="1" title="Qty" class="input-text qty text" size="4"></div>
                        <button type="submit" class="single_add_to_cart_button button alt">Add to cart</button>
                    </form>
                </div>
                <div class="share-post">
                    <a href="#" class="share-text">Share</a>
                    <ul class="social-icon">
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- /col-sm-7 -->
            <div class="despric">
                <div class="col-md-12">
                    <!-- Nav tabs -->
                    <ul id="tabs" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#Description" role="tab" data-toggle="tab">Description</a></li>
                        <li role="presentation"><a href="#review" role="tab" data-toggle="tab">Reviews (2)</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="Description">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac elementum elit. Morbi eu arcu ipsum. Aliquam lobortis accumsan quam ac convallis.
                                Fusce elit mauris, aliquet at odio vel, convallis vehicula nisi. Morbi vitae porttitor dolor. Integer eget metus sem. Nam venenatis mauris vel leo pulvinar,
                                id rutrum dui varius. Nunc ac varius quam, non convallis magna. Donec suscipit commodo dapibus. Vestibulum et ullamcorper ligula. Morbi bibendum tempor rutrum.
                                Pelle tesque auctor purus id molestie ornare.Donec eu lobortis risus. Pellentesque sed aliquam lorem. Praesent pulvinar lorem vel mauris ultrices posuere.
                                Phasellus elit ex, gravida a semper ut, venenatis vitae diam. Nullam eget leo massa. Aenean eu consequat arcu, vitae scelerisque quam. Suspendisse risus turpis,
                                pharetra a finibus vitae, lobortis et mi.
                            </p>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="review">
                            <h3>Reviews (2)</h3>
                            <div class="commant-text row">
                                <div class="col-lg-1 col-md-1 col-sm-4 col-xs-hidden">
                                    <div class="profile">
                                        <img class="img-responsive" src="images/client1.jpg" alt="#">
                                    </div>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-8 col-xs-hidden">
                                    <h5>David</h5>
                                    <p><span class="c_date">March 2, 2016</span> | <span><a rel="nofollow" class="comment-reply-link" href="#">Reply</a></span></p>
                                    <span class="rating">
                                 <i class="fa fa-star" aria-hidden="true"></i>
                                 <i class="fa fa-star" aria-hidden="true"></i>
                                 <i class="fa fa-star" aria-hidden="true"></i>
                                 <i class="fa fa-star" aria-hidden="true"></i>
                                 <i class="fa fa-star-o" aria-hidden="true"></i>
                                 </span>
                                    <p class="msg">ThisThis book is a treatise on the theory of ethics, very popular during the Renaissance.
                                        The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet..
                                    </p>
                                </div>
                            </div>
                            <div class="commant-text row">
                                <div class="col-lg-1 col-md-1 col-sm-4 col-xs-hidden">
                                    <div class="profile">
                                        <img class="img-responsive" src="images/client2.jpg" alt="#">
                                    </div>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-8 col-xs-hidden">
                                    <h5>Jack</h5>
                                    <p><span class="c_date">March 2, 2016</span> | <span><a rel="nofollow" class="comment-reply-link" href="#">Reply</a></span></p>
                                    <span class="rating">
                                 <i class="fa fa-star" aria-hidden="true"></i>
                                 <i class="fa fa-star" aria-hidden="true"></i>
                                 <i class="fa fa-star" aria-hidden="true"></i>
                                 <i class="fa fa-star" aria-hidden="true"></i>
                                 <i class="fa fa-star-o" aria-hidden="true"></i>
                                 </span>
                                    <p class="msg">Nunc augue purus, posuere in accumsan sodales ac, euismod at est. Nunc faccumsan ermentum consectetur metus placerat mattis. Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat. Nam vitae imperdiet turpis</p>
                                </div>
                            </div>
                            <div class="well well-sm">
                                <div class="text-right">
                                    <a class="btn btn-success btn-green" href="#reviews-anchor" id="open-review-box">Leave a Review</a>
                                </div>
                                <div class="row" id="post-review-box" style="display:none;">
                                    <div class="col-md-12">
                                        <form accept-charset="UTF-8" action="#" method="post">
                                            <input id="ratings-hidden" name="rating" type="hidden">
                                            <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5"></textarea>
                                            <div class="text-right">
                                                <div class="stars starrr" data-rating="0"></div>
                                                <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">
                                                    <span class="glyphicon glyphicon-remove"></span>Cancel</a>
                                                <button class="btn btn-success btn-lg" type="submit">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- RELATED PRODUCTS -->
            <div class="section wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                <div class="container">
                    <div class="section-title">
                        <i class="flaticon-shovel"></i>
                        <h3>Related Products</h3>
                        <p class="lead">A List of Garden Supplies from the Shop</p>
                    </div>
                    <!-- end title -->
                    <div class="row text-center">
                        <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="uploads/shop_01.html" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$21.00</a></small>
                                    <h4><a href="shop-single.html">Flower Pots</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                        <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="uploads/shop_02.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$9.00</a></small>
                                    <h4><a href="shop-single.html">Watering Machine</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                        <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="uploads/shop_03.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$8.00</a></small>
                                    <h4><a href="shop-single.html">Small Shovel</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                        <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="uploads/shop_04.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$8.00</a></small>
                                    <h4><a href="shop-single.html">Small Shovel</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-button text-center">
                                <a href="{{route('sellerShop',$item->producible->identity->identifier)}}" class="btn btn-primary btn-lg">Visit Shop <i class="fa fa-angle-right"></i></a>
                            </div>
                            <!-- end section-button -->
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                </div>
                <!-- end container -->
            </div>
            <!-- END RELATED PRODUCTS -->
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end section -->
@endsection

@section('custom-script')

    <script>

    </script>
@endsection
