@extends('layouts.main')

@section('body-class')
    home-page
@endsection

@section('main-content')


    <div class="slider-section">
        <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classicslider1" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
            <!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
            <div id="rev_slider_4_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
                <ul>
                    <li data-index="rs-1812" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="{{asset('images/slider_03.jpg')}}"  data-rotate="0"  data-saveperformance="off"  data-title="Greenhouse Building" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{asset('images/slider_03.jpg')}}"  alt=""  data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                        <!-- LAYER NR. BG -->
                        <div class="tp-caption tp-shape tp-shapewrapper   rs-parallaxlevel-0"
                             id="slide-270-layer-1012"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                             data-width="full"
                             data-height="full"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                             data-transform_out="s:300;s:300;"
                             data-start="750"
                             data-basealign="slide"
                             data-responsive_offset="on"
                             data-responsive="off"
                             style="z-index: 5;background-color:rgba(0, 0, 0, 0.25);border-color:rgba(0, 0, 0, 0.50);">
                        </div>
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-shape tp-shapewrapper   tp-resizeme rs-parallaxlevel-0"
                             id="slide-18-layer-912"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['15','15','15','15']"
                             data-width="500"
                             data-height="140"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:0px;"
                             data-mask_out="x:inherit;y:inherit;"
                             data-start="2000"
                             data-responsive_offset="on"
                             style="z-index: 5;background-color:rgba(29, 29, 29, 0.85);border-color:rgba(0, 0, 0, 0.50);">
                        </div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0"
                             id="slide-18-layer-112"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                             data-fontsize="['70','70','70','35']"
                             data-lineheight="['70','70','70','50']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1000"
                             data-splitin="chars"
                             data-splitout="none"
                             data-responsive_offset="on"
                             data-elementdelay="0.05"
                             style="z-index: 6; white-space: nowrap;">Greenhouse Building
                        </div>
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0"
                             id="slide-18-layer-412"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['52','51','51','31']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1500"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 7; white-space: nowrap;">AVAILABLE ON Farmgle
                        </div>
                    </li>
                    <li data-index="rs-181" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="{{asset('images/slider_02.jpg')}}"  data-rotate="0"  data-saveperformance="off"  data-title="Flower & Tree Planting" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{asset('images/slider_02.jpg')}}"  alt=""  data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                        <!-- LAYER NR. BG -->
                        <div class="tp-caption tp-shape tp-shapewrapper   rs-parallaxlevel-0"
                             id="slide-270-layer-101"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                             data-width="full"
                             data-height="full"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                             data-transform_out="s:300;s:300;"
                             data-start="750"
                             data-basealign="slide"
                             data-responsive_offset="on"
                             data-responsive="off"
                             style="z-index: 5;background-color:rgba(0, 0, 0, 0.25);border-color:rgba(0, 0, 0, 0.50);">
                        </div>
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-shape tp-shapewrapper   tp-resizeme rs-parallaxlevel-0"
                             id="slide-18-layer-91"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['15','15','15','15']"
                             data-width="500"
                             data-height="140"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:0px;"
                             data-mask_out="x:inherit;y:inherit;"
                             data-start="2000"
                             data-responsive_offset="on"
                             style="z-index: 5;background-color:rgba(29, 29, 29, 0.85);border-color:rgba(0, 0, 0, 0.50);">
                        </div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0"
                             id="slide-18-layer-11"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                             data-fontsize="['70','70','70','35']"
                             data-lineheight="['70','70','70','50']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1000"
                             data-splitin="chars"
                             data-splitout="none"
                             data-responsive_offset="on"
                             data-elementdelay="0.05"
                             style="z-index: 6; white-space: nowrap;">Flower & Tree Planting
                        </div>
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0"
                             id="slide-18-layer-41"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['52','51','51','31']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1500"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 7; white-space: nowrap;">AVAILABLE ON Farmgle
                        </div>
                    </li>
                    <li data-index="rs-18" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="{{asset('images/slider_01.jpg')}}"  data-rotate="0"  data-saveperformance="off"  data-title="Landscaped Garden" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{asset('images/slider_01.jpg')}}"  alt=""  data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                        <!-- LAYER NR. BG -->
                        <div class="tp-caption tp-shape tp-shapewrapper   rs-parallaxlevel-0"
                             id="slide-270-layer-10"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                             data-width="full"
                             data-height="full"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                             data-transform_out="s:300;s:300;"
                             data-start="750"
                             data-basealign="slide"
                             data-responsive_offset="on"
                             data-responsive="off"
                             style="z-index: 5;background-color:rgba(0, 0, 0, 0.25);border-color:rgba(0, 0, 0, 0.50);">
                        </div>
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-shape tp-shapewrapper   tp-resizeme rs-parallaxlevel-0"
                             id="slide-18-layer-9"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['15','15','15','15']"
                             data-width="500"
                             data-height="140"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:0px;"
                             data-mask_out="x:inherit;y:inherit;"
                             data-start="2000"
                             data-responsive_offset="on"
                             style="z-index: 5;background-color:rgba(29, 29, 29, 0.85);border-color:rgba(0, 0, 0, 0.50);">
                        </div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0"
                             id="slide-18-layer-1"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                             data-fontsize="['70','70','70','35']"
                             data-lineheight="['70','70','70','50']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1000"
                             data-splitin="chars"
                             data-splitout="none"
                             data-responsive_offset="on"
                             data-elementdelay="0.05"
                             style="z-index: 6; white-space: nowrap;">Landscaped Garden
                        </div>
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0"
                             id="slide-18-layer-4"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['52','51','51','31']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1500"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 7; white-space: nowrap;">AVAILABLE ON Farmgle
                        </div>
                    </li>
                </ul>
                <div class="tp-static-layers"></div>
            </div>
        </div>
        <!-- END REVOLUTION SLIDER -->
    </div>
    <!-- end slider -->
    <!-- add new section -->
    <section class="about-us wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <div class="img-border">
                        <img src="{{asset('images/team_02.jpg')}}" alt="Image">
                    </div>
                </div>
                <div class="col-lg-5 col-md-8 col-sm-12 col-xs-12">
                    <h2>We are <b>Farmgle Group</b><br>DESIGN AND CONSTRUCTION</h2>
                    <p class="lead">A Professional Gardening Service for Ever!</p>
                    <p>Lorem ipsum dolor sit amet, cons ectetur elit. Vestibulum nec odios Suspe ndisse cursus mal suada faci lisis. Lorem ipsum dolor sit ametion consectetur elit. Vesti bulum nec odio ipsum.</p>
                    <ul>
                        <li><i class="fa fa-angle-right"></i> Lawn Renovation<br></li>
                        <li><i class="fa fa-angle-right"></i> Landscape Lighting<br></li>
                        <li><i class="fa fa-angle-right"></i> Flowerscaping<br></li>
                        <li><i class="fa fa-angle-right"></i> Natural Stone Walks</li>
                    </ul>
                    <ul>
                        <li><i class="fa fa-angle-right"></i> Lawn Maintenance<br></li>
                        <li><i class="fa fa-angle-right"></i> Seasonal Clean-ups<br></li>
                        <li><i class="fa fa-angle-right"></i> Snow Plowing &amp; Salting<br></li>
                        <li><i class="fa fa-angle-right"></i> Tree &amp; Shrub Care</li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col-lg-offset-0 col-sm-offset-0 col-xs-offset-0 lawn-maintenance">
                    <h3>Lawn <b>maintenance</b></h3>
                    <p>Lorem ipsum dolor sit amet, cons ectetur elit. Vestibulum nec odios Suspe ndisse cursus mal suada faci lisis. Lorem ipsum dolor sit ametion consectetur. </p>
                    <img class="img-responsive" src="{{asset('images/page_parallax_01.jpg')}}" alt="Image">
                </div>
            </div>
        </div>
    </section>
    <!-- end add new section -->

    <div class="section wow fadeIn">
        <div class="container">
            <div class="section-title">
                <i class="flaticon-shovel"></i>
                <h3>Garden Supplies</h3>
                <p class="lead">A List of Garden Supplies from the Shop</p>
            </div>
            <!-- end title -->
            <div class="row text-center">
                @if($identifiable->products->count() != 0)
                @foreach($identifiable->products as $product)
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro">
                    <div class="shop-wrapper">
                        <div class="post-media entry">
                            <img src="{{$product->getFirstMediaUrl('product','grid')}}" alt="" class="img-responsive img-thumbnail">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                </div>
                            </div>
                        </div>
                        <!-- end media -->
                        <div class="shop-meta">
                            <small><a href="{{route('singleProduct',[$identifiable->identity->identifier,$product->uuid])}}">{{$product->getLocalPrice()}}</a></small>
                            <h4><a href="{{route('singleProduct',[$identifiable->identity->identifier,$product->uuid])}}">{{$product->product}}</a></h4>
                        </div>
                        <!-- end shop-meta -->
                    </div>
                    <!-- end shop-wrapper -->
                </div>
                <!-- end col -->
                @endforeach
                @else
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro">
                    <div class="shop-wrapper">
                        <div class="post-media entry">
                            <img src="{{asset('images/shop_02.jpg')}}" alt="" class="img-responsive img-thumbnail">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                </div>
                            </div>
                        </div>
                        <!-- end media -->
                        <div class="shop-meta">
                            <small><a href="shop-single.html">$9.00</a></small>
                            <h4><a href="shop-single.html">Watering Machine</a></h4>
                        </div>
                        <!-- end shop-meta -->
                    </div>
                    <!-- end shop-wrapper -->
                </div>
                <!-- end col -->
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro">
                    <div class="shop-wrapper">
                        <div class="post-media entry">
                            <img src="{{asset('images/shop_03.jpg')}}" alt="" class="img-responsive img-thumbnail">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                </div>
                            </div>
                        </div>
                        <!-- end media -->
                        <div class="shop-meta">
                            <small><a href="shop-single.html">$8.00</a></small>
                            <h4><a href="shop-single.html">Small Shovel</a></h4>
                        </div>
                        <!-- end shop-meta -->
                    </div>
                    <!-- end shop-wrapper -->
                </div>
                <!-- end col -->
                @endif
            </div>
            <!-- end row -->
            {{--<div class="row">
                <div class="col-md-12">
                    <div class="section-button text-center">
                        <a href="#" class="btn btn-primary btn-lg">Visit Shop <i class="fa fa-angle-right"></i></a>
                    </div>
                    <!-- end section-button -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->--}}
        </div>
        <!-- end container -->
    </div>

@endsection

@section('custom-script')

    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="{{asset('revolution/js/jquery.themepunch.tools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
@endsection
