@extends('layouts.main')
@section('body-class')
    Shop-Layout-page
@endsection


@section('main-content')

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 inner-page-banner wow fadeIn">
        <div class="container">
            <h4>Market</h4>
            <ul class="breadcrumbs">
                <li><a href="index-2.html">Home</a></li>
                <li>/</li>
                <li class="active">Market</li>
            </ul>
        </div>
    </div>
    <!-- end section -->
    <div class="section single-blog bottom-grass wow fadeIn">
        <div class="container">
            <!-- blog contact -->

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-right">
                <aside class="side-bar">
                    {{--<div class="search-box">
                        <div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" class="  search-query form-control" placeholder="Search" />
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" type="button">
                                        <span class=" glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>--}}
                    <div class="side-bar-blog">
                        <h3>Popular News & Tips</h3>
                        <ul>
                            <li>
                                <img class="post-thumb" src="{{asset('images/img-cap4.jpg')}}" alt="" />
                                <div class="content-wrap">
                                    <h5>3 Reasons to Build A Backyard Fire Pit</h5>
                                    <span class="date">8 Nov 2017</span>
                                </div>
                            </li>
                            <li>
                                <img class="post-thumb" src="{{asset('images/img-cap5.jpg')}}" alt="" />
                                <div class="content-wrap">
                                    <h5>3 Reasons to Build A Backyard Fire Pit</h5>
                                    <span class="date">8 Nov 2017</span>
                                </div>
                            </li>
                            <li>
                                <img class="post-thumb" src="{{asset('images/img-cap6.jpg')}}" alt="" />
                                <div class="content-wrap">
                                    <h5>3 Reasons to Build A Backyard Fire Pit</h5>
                                    <span class="date">8 Nov 2017</span>
                                </div>
                            </li>
                            <li>
                                <img class="post-thumb" src="{{asset('images/img-cap4.jpg')}}" alt="" />
                                <div class="content-wrap">
                                    <h5>3 Reasons to Build A Backyard Fire Pit</h5>
                                    <span class="date">8 Nov 2017</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="side-bar-blog">
                        <h3>About gardener</h3>
                        <div class="white-blog">
                            <p>Gardener Company provide the design and installation of landscape construction projects including walkways, steps, retaining walls, patios,
                                veneer stone, planting, trans-planting,drainage systems, lawn renovation and installation, paver driveways, cobblestone edging, and excavation.</p>
                        </div>
                    </div>
                    <div class="side-bar-blog catargy">
                        <h3>Categories</h3>
                        <ul class="wht">
                            <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Blog</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Blog 2 Col</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Construction Tips</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Outside</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Uncategorized</a></li>
                        </ul>
                    </div>
                    <div class="side-bar-blog catargy">
                        <h3>TAGS WIDGET</h3>
                        <a class="tag" href="#">Drain Cleaning</a><a class="tag" href="#">Kitchen Plumbing</a><a class="tag" href="#">outide Plumbing</a><a class="tag" href="#">Pipe fixes</a>
                        <a class="tag" href="#">Pipe leakages</a><a class="tag" href="#">tips</a>
                    </div>
                </aside>
            </div>

            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

                <div class="section-title">
                    <i class="flaticon-shovel"></i>
                    <h3>Garden Supplies</h3>
                </div>

                <div class="row text-center">
                    @if($products->count() !== 0)
                    @foreach($products as $product)
                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="{{$product->getFirstMediaUrl('product','grid')}}" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="{{route('singleProduct',[$product->producible->identity->identifier,$product->uuid])}}">{{$product->getLocalPrice()}}</a></small>
                                    <h4><a href="{{route('singleProduct',[$product->producible->identity->identifier,$product->uuid])}}">{{$product->product}}</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                    @endforeach
                        @else

                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="images/shop_02.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$9.00</a></small>
                                    <h4><a href="shop-single.html">Watering Machine</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="images/shop_03.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$8.00</a></small>
                                    <h4><a href="shop-single.html">Small Shovel</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="images/shop_04.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$8.00</a></small>
                                    <h4><a href="shop-single.html">Small Shovel</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="images/shop_01.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$21.00</a></small>
                                    <h4><a href="shop-single.html">Flower Pots</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="images/shop_02.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$9.00</a></small>
                                    <h4><a href="shop-single.html">Watering Machine</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="images/shop_03.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$8.00</a></small>
                                    <h4><a href="shop-single.html">Small Shovel</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="images/shop_04.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$8.00</a></small>
                                    <h4><a href="shop-single.html">Small Shovel</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="images/shop_01.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$21.00</a></small>
                                    <h4><a href="shop-single.html">Flower Pots</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="images/shop_02.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$9.00</a></small>
                                    <h4><a href="shop-single.html">Watering Machine</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="images/shop_03.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$8.00</a></small>
                                    <h4><a href="shop-single.html">Small Shovel</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                        <!-- end col -->
                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn skew-pro" style="visibility: visible; animation-name: fadeIn;">
                            <div class="shop-wrapper">
                                <div class="post-media entry">
                                    <img src="images/shop_04.jpg" alt="" class="img-responsive img-thumbnail">
                                    <div class="magnifier">
                                        <div class="buttons">
                                            <a class="st" rel="bookmark" href="shop-cart.html"><span class="flaticon-shop"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end media -->
                                <div class="shop-meta">
                                    <small><a href="shop-single.html">$8.00</a></small>
                                    <h4><a href="shop-single.html">Small Shovel</a></h4>
                                </div>
                                <!-- end shop-meta -->
                            </div>
                            <!-- end shop-wrapper -->
                        </div>
                    @endif

                </div>



            </div>

            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end section -->
@endsection

@section('custom-script')


@endsection
