@extends('layouts.main')
@section('body-class')
    shopping-cart checkout
@endsection

@section('main-content')
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-justify mt-auto">
                        <i class="flaticon-shovel"></i>
                        <h3>Please select the criterion that best fits your needs</h3>
                    </div>
                </div>
            </div>


            <div class="row text-center">
                <div class="col-md-12">
                    <form method="post" action="{{route('criterion')}}">
                        @csrf
                        <fieldset class="row appointment_form">

                            <div class="col-md-12">
                                <div class="form-field">
                                    <label for="select" class="sr-only">Please select the criterion that best fits you</label>
                                    <select id="select" class="selectpicker" name="user_type" data-style="btn-white">
                                        <option value="customer"> Regular Customer </option>
                                        <option value="institution"> Institution </option>
                                        <option value="manufacturer"> Manufacturer </option>
                                        <option value="farmer"> Farmer </option>
                                        <option value="professional"> Freelance Professional </option>
                                        <option value="logistics"> Logistics Services </option>
                                    </select>

                                    <small>Remember you cannot change this setting until you are done with the registration</small>

                                </div>
                            </div>

                            <br>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-lg">Continue</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>



        </div>
    </div>


@endsection

