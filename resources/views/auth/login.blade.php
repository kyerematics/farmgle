@extends('layouts.main')
@section('body-class')
shopping-cart checkout
@endsection
@section('main-content')
<div class="section bottom-grass wow fadeIn">
    <div class="container-fluid">

        <div class="payment-form">
            <div class="col-xs-12 col-md-4 pull-right">

                <div class="panel panel-default credit-card-box">
                    <div class="panel-heading display-table" >
                        <div class="row display-tr" >
                            <h3 class="panel-title display-td" >Sign In</h3>

                        </div>
                    </div>
                    <div class="panel-body">
                        <form id="payment-form" method="POST" action="{{route('login')}}" >
                            @csrf
                            <div class="row no-gutters">
                                <div class="col-xs-12">
                                    <div class="form-field">
                                        <label>Email</label>
                                        <div class="form-field">
                                                <input id="l_email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback " role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                @endif
                                            {{--<span class="input-group-addon"><i class="fa fa-envelope"></i></span>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row no-gutters">
                                <div class="col-xs-12">
                                    <div class="form-field">
                                        <label>Password</label>
                                        <input id="l_password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-xs-12 col-md-9 payment-bt">
                                    <button class="subscribe btn btn-success btn-lg " type="submit">Sign In</button>
                                </div>

                                        @if (Route::has('password.request'))
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        @endif

                            </div>
                            <div class="row">
                                OR Sign in with <i class="fa fa-2x fa-facebook-official"></i> &nbsp; <i
                                        class="fa fa-2x fa-google"></i>
                            </div>

                        </form>
                        <div class="row">
                            Don't have an account? <a href="#register">Click Here</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="payment-form no-gutters" id="register">
            <div class="col-xs-12 col-md-8">

                <div class="panel panel-default credit-card-box">
                    <div class="panel-heading display-table" >
                        <div class="row display-tr" >
                            <h3 class="panel-title display-td" >Create Account</h3>

                        </div>
                    </div>
                    <div class="panel-body">
                        <link rel="stylesheet" href="{{asset('css/intlTelInput.min.css',true)}}">
                        <form method="POST" action="{{ route('register') }}" >
                            @csrf
                            <fieldset>
                                <div class="col-md-6">
                                    <div class="form-field">
                                        <label>First name <span class="red">*</span></label>
                                        <input id="firstName" type="text" class="{{ $errors->has('firstName') ? ' is-invalid' : '' }}" name="firstName" value="{{ old('firstName') }}" required autofocus>

                                        @if ($errors->has('firstName'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstName') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-field">
                                        <label>Last name <span class="red">*</span></label>
                                        <input id="lastName" type="text" class="{{ $errors->has('lastName') ? ' is-invalid' : '' }}" name="lastName" value="{{ old('lastName') }}" required autofocus>

                                        @if ($errors->has('lastName'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastName') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-7">
                                    <div class="form-field">
                                        <label>Email address <span class="red">*</span></label>
                                        <input id="r_email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback " role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-field">
                                        <label>Phone <span class="green">*</span></label>
                                        <input type="tel" name="phone" id="phone" class="{{ $errors->has('full_phone') ? ' is-invalid' : '' }}" value=" {{ old('full_phone') }}" required>
                                        <span id="valid-msg" class="hide"><i class="fa fa-check-circle-o"></i>Valid</span>
                                        <span id="error-msg" class="hide"></span>
                                        @if ($errors->has('full_phone'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('full_phone') }}</strong>
                                    </span>
                                        @endif

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-field">
                                        <label>Password <span class="red">*</span></label>
                                        <input id="r_password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-field">
                                        <label>Confirm Password <span class="red">*</span></label>
                                        <input id="password-confirm" type="password" name="password_confirmation" required>
                                    </div>
                                </div>

                                <div class="clearfix form-group row">
                                    <div class="col-md-6">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="terms" id="terms" {{ old('terms') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                I agree with the <a href="">Terms and Conditions</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix row">
                                    <div class="col-xs-12 col-md-9 payment-bt">
                                        <button class="subscribe btn btn-success btn-lg " type="submit">Register</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h2>CREATE ACCOUNT</h2>
                </div>
                <div class="col-md-12">
                    <div class="checkout-form" id="register">
                        <form method="POST" action="{{ route('register') }}" >
                            <fieldset>
                                <div class="col-md-6">
                                    <div class="form-field">
                                        <label>First name <span class="red">*</span></label>
                                        <input id="firstName" type="text" class="{{ $errors->has('firstName') ? ' is-invalid' : '' }}" name="name" value="{{ old('firstName') }}" required autofocus>

                                        @if ($errors->has('firstName'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstName') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-field">
                                        <label>Last name <span class="red">*</span></label>
                                        <input id="lastName" type="text" class="{{ $errors->has('lastName') ? ' is-invalid' : '' }}" name="lastName" value="{{ old('lastName') }}" required autofocus>

                                        @if ($errors->has('lastName'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastName') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-8">
                                    <div class="form-field">
                                        <label>Email address <span class="red">*</span></label>
                                        <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-field">
                                        <label>Phone <span class="red">*</span></label>
                                        <input type="text" name="ph" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-field">
                                        <label>Password <span class="red">*</span></label>
                                        <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-field">
                                        <label>Confirm Password <span class="red">*</span></label>
                                        <input id="password-confirm" type="password" name="password_confirmation" required>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xs-12 payment-bt">
                                        <button class="subscribe btn btn-success btn-lg btn-block" type="button">Register</button>
                                    </div>
                                </div>
                                <div class="row" style="display:none;">
                                    <div class="col-xs-12">
                                        <p class="payment-errors"></p>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>

            </div>
        </div>--}}
    </div>
    <!-- end container -->
</div>

@endsection
@section('custom-script')
    <script src="{{asset('js/intlTelInput.min.js')}}"></script>
    <script src="{{asset('js/utils.js')}}"></script>
    <script>
        /*var input = document.querySelector("#phone");
        window.intlTelInput(input);*/

        var input = document.querySelector("#phone"),
            errorMsg = document.querySelector("#error-msg"),
            validMsg = document.querySelector("#valid-msg");

        // here, the index maps to the error code returned from getValidationError - see readme
        var errorMap = [ "Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];


        var reset = function() {
            input.classList.remove("error");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };

        // on blur: validate
        input.addEventListener('blur', function() {
            reset();
            if (input.value.trim()) {
                if (iti.isValidNumber()) {
                    validMsg.classList.remove("hide");
                } else {
                    input.classList.add("error");
                    var errorCode = iti.getValidationError();
                    errorMsg.innerHTML = errorMap[errorCode];
                    errorMsg.classList.remove("hide");
                }
            }
        });

        // on keyup / change flag: reset
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);

        // initialise plugin
        var iti = window.intlTelInput(input, {
            initialCountry: "auto",
            hiddenInput: "full_phone",
            geoIpLookup: function(callback) {
                $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            utilsScript: "{{asset('js/utils.js')}}"
        });


    </script>
    @endsection
