<!DOCTYPE html>
<html lang="en">



@include('layouts._partials.head')

<body class="@yield('body-class')">
<!-- LOADER -->
<div id="preloader">
    <img class="preloader" src="{{asset('images/preloader.gif')}}" alt="#" />
</div>
<!-- END LOADER -->
<div id="wrapper">
    {{--<div class="topbar-panel panel panel-primary hidden-xs wow fadeIn">
        <span class="clickable"><i class="fa fa-angle-up"></i></span>
        <div class="panel-body">
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <p>Welcome to the <a href="{{route('timeline')}}">Farmgle</a>. We offer landscaping for industrial clients.</p>
                        </div>
                        <!-- end left -->
                       --}}{{-- <div class="col-md-6 text-right payment-bt">
                            <button class="subscribe btn btn-success btn-xs" type="button">Login/Register</button>
                        </div>--}}{{--

                        <!-- end left -->
                    </div>
                    <!-- end row -->
                </div>
                <!-- enc ontainer -->
            </div>
            <!-- end topbar -->
        </div>
    </div>--}}

    @include('layouts._partials.header')
    <!-- end header -->
    @includeWhen((request()->fullUrl() !== route('login'))== true,'layouts._partials.search')

    @yield('main-content')

    <!-- end section -->
    @include('layouts._partials.footer')
    <!-- end copyrights -->
</div>
<!-- end wrapper -->
<script src="{{asset('js/all.js',true)}}"></script>
<!-- ALL PLUGINS -->
<script src="{{asset('js/custom.js',true)}}"></script>
@hasSection('rev-script')
@include('layouts._partials.script')
    @endif

@yield('custom-script')
</body>

</html>
