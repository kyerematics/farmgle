<header class="header wow fadeIn">
    <div class="container menu-margin">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <a class="navbar-brand" href="{{route('timeline')}}"><img src="{{asset('images/logo.png')}}" alt="Farmgle-logo"></a>
            </div>
            <!-- end col -->

            <div class="main-menu">
                <a href="#" class="hidden-sm hidden-md hidden-lg visible-xs pull-right clearfix menu-cart clickable"><i class="fa fa-2x fa-shopping-cart"></i></a>
                <nav class="navbar navbar-default">
                    <div class="navbar-header">

                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li><a class="active" href="{{route('timeline')}}">Home</a></li>
                            <li><a href="{{route('allProducts')}}">Market</a></li>
                            <li><a href="{{route('allServices')}}">Services</a></li>
                            {{--<li class="dropdown hasmenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages <span class="fa fa-angle-down"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="page-members.html">Team of Gardeners</a></li>
                                    <li><a href="page-testimonials.html">Testimonials</a></li>
                                    <li><a href="page-pricing.html">Pricing & Plan</a></li>
                                    <li><a href="single.html">Single Post</a></li>
                                    <li><a href="page-404.html">Not Found</a></li>
                                </ul>
                            </li>
                            <li class="dropdown hasmenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Shop <span class="fa fa-angle-down"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="shop.html">Shop Layout</a></li>
                                    <li><a href="shop-single.html">Shop Single</a></li>
                                    <li><a href="shop-checkout.html">Shop Checkout</a></li>
                                    <li><a href="shop-cart.html">Shopping Cart</a></li>
                                </ul>
                            </li>--}}
                            <li><a href="{{route('allPosts')}}">Blog</a></li>
                            {{--<li><a href="#">Cart</a></li>
                            <li><a href="#">Search</a></li>--}}
                            @guest()
                            <li><a href="{{route('login')}}">Log in/Sign Up</a></li>
                           {{-- <li><a href="{{route('login')}}#register">Sign up</a></li>--}}
                            @endguest
                            @auth()

                                <li class="dropdown hasmenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hi, {{auth()->user()->firstName}} <span class="fa fa-angle-down"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{route('home')}}">Profile</a></li>
                                        <li><a href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a></li>
                                    </ul>
                                </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                            @endauth
                            <li class="hidden-xs"><a href="#"><i class="fa fa-2x fa-shopping-cart"></i></a></li>
                        </ul>
                    </div>

                </nav>
                <!-- end navbar -->
            </div>

        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
   {{-- <div class="information">
        <div class="container">
            <div class="col-md-4 col-sm-4 hidden-xs infor">
                <div class="header-contact clearfix">
                    <p><i class="flaticon-placeholder alignleft"></i><span> 007 Edgar Buildings,<br>George Street, CA 03, USA</span></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs infor infor-design">
                <div class="header-contact clearfix">
                    <p><i class="flaticon-clock alignleft"></i><span> Mon - Sat 9.00 - 20.00,<br>Sunday CLOSED</span></p>
                </div>
                <!-- end header-contact -->
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs infor">
                <div class="header-contact clearfix">
                    <p><i class="flaticon-phone-call alignleft"></i><span> +1 888 122 9000,<br>Call us for enquiry</span></p>
                </div>
                <!-- end header-contact -->
            </div>
            <!-- end col -->
        </div>
    </div>--}}
</header>
