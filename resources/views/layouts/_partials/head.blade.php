<head>
    <meta charset="utf-8">
    <title>{{config('app.name')}} - Responsive Website Template</title>
    <!-- Stylesheets -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css',true)}}">
    <!-- Site CSS -->
    <link rel="stylesheet" href="{{asset('style.css',true)}}">
    <!-- Colors CSS -->
    <link rel="stylesheet" href="{{asset('css/colors.css',true)}}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/custom.css',true)}}">
    <!-- revolution slider css -->
    <link rel="stylesheet" type="text/css" href="{{asset('revolution/css/settings.css',true)}}">
    <link rel="stylesheet" type="text/css" href="{{asset('revolution/css/layers.css',true)}}">
    <link rel="stylesheet" type="text/css" href="{{asset('revolution/css/navigation.css',true)}}">
    <!--Favicon-->
    <link rel="shortcut icon" href="{{asset('images/favicon.png',true)}}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{asset('images/apple-touch-icon.png',true)}}">
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/responsive.css',true)}}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
