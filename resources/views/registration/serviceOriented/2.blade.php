
<fieldset>
    <div class="col-md-12">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    <div class="col-md-12">
        <div class="form-field">
            <label>Tell us who you are <span class="red">*</span></label>
            <textarea name="description" placeholder="A brief description about your profession" minlength="50" >

            </textarea>

        </div>
    </div>
     <div class="col-md-12">
            <div class="form-field">
                <label>Your mission <span class="red">*</span></label>
                <textarea name="mission" placeholder="A statement of your mission" minlength="50" >

                </textarea>

            </div>
        </div>
     <div class="col-md-12">
            <div class="form-field">
                <label>Your Vision <span class="red">*</span></label>
                <textarea name="vision" placeholder="A statement of your vision" minlength="50" >

                </textarea>

            </div>
        </div>
    <div class="col-md-12">
                <div class="form-field">
                    <label>What makes you stand out? <span class="red">*</span></label>
                    <textarea name="your_why" placeholder="Tell us what makes you stand out?" minlength="50" >

                    </textarea>

                </div>
            </div>


</fieldset>
