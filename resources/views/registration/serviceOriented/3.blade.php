<fieldset>
    <div class="col-md-12">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-field">
            <label for="">Title of Service provided</label>
            <input type="text" name="title[]" placeholder="The services you provide" required>
            <small>The services/packages you provide for your clients</small>
        </div>
        <div class="form-field">
            <label for="">Description of Service</label>
            <textarea name="description[]" id="" cols="30" rows="10" required></textarea>
            <small>A brief description of the service you provide</small>
        </div>

        <p>A button is needed to create services</p>
        <div class="form-field">
            <label for="">Cover Image</label>
            <input type="file" accept="image/*" name="cover_image">

        </div>
        <div class="form-field">
            <label for="">Image</label>
            <input type="file" accept="image/*" name="media[]" multiple>
            <small>Media(images) of service if available. Multi-upload supported</small>
        </div>


    </div>

</fieldset>

