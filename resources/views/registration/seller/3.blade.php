{{--Product upload section--}}
<div class="clearfix">
    <div class= id="dynamic_field">
        <div id="1">


    <div>
        <h4>Product Upload</h4>
        <small>You can upload all your products now or you can choose to upload them later</small>
    </div>

    @if ($errors->any())
        <div class="col-md-12">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>

    @endif
    <div class="col-md-12">

        <div class="form-field">
            <label>Product Name <span class="red">*</span></label>
            <input type="text" name="product[]" placeholder="Enter the name of your product" />
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-field">
            <label>Product Description <span class="red">*</span></label>
            <textarea name="description[]" id="" cols="30" rows="10" placeholder="Enter the product description"></textarea>
        </div>
    </div>

    <div class="col-md-6">

            <div class="form-field">
                <label>Price <span class="red">*</span></label>
                <input type="number" min="0" name="price[]" placeholder="Enter the price of the product" />
            </div>

    </div>
    {{--<div class="col-md-6">
        <div class="form-field">
            <label>Quantity</label>
            <div class="form-field cardNumber">
                <input type="number" min="0" name="quantity" placeholder="Enter the quantity of the product" />
                <div class="input-group-addon">
                    <select name="unit" id="select">
                        <option value="">units</option>
                    <option value="">Kilograms</option>
                    <option value="">grams</option>
                    <option value="">tonnes</option>
                    <option value="">litres</option>
                </select>
                </div>
            </div>
        </div>
    </div>--}}
    <div class="col-md-6">
        <div class="col-md-4">
            <div class="form-field">
                <label>Quantity <span class="red">*</span></label>
                <input type="number" min="0" name="quantity[]" placeholder="Enter the quantity of the product" />
            </div>
        </div>
        <div class="col-md-2" >
            <div class="form-field form-inline">
                <label for="select" title="unit">Units</label>
                <select name="unit[]" id="select">unit
                    <option value="pieces">units</option>
                    <option value="kilogram">Kilograms</option>
                    <option value="grams">grams</option>
                    <option value="tonnes">tonnes</option>
                    <option value="litres">litres</option>
                </select>

            </div>
        </div>

    </div>
    <div class="col-md-12">
        <div class="form-field">
            <label>Images of product <span class="red">*</span></label>
            <input type="file" accept="image/*" name="media[0][]" placeholder="Upload all the images of the product" multiple/>
        </div>
    </div>

            {{--Duplicate--}}
<div class="col-md-12">

        <div class="form-field">
            <label>Product Name <span class="red">*</span></label>
            <input type="text" name="product[]" placeholder="Enter the name of your product" />
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-field">
            <label>Product Description <span class="red">*</span></label>
            <textarea name="description[]" id="" cols="30" rows="10" placeholder="Enter the product description"></textarea>
        </div>
    </div>

    <div class="col-md-6">

            <div class="form-field">
                <label>Price <span class="red">*</span></label>
                <input type="number" min="0" name="price[]" placeholder="Enter the price of the product" />
            </div>

    </div>
    {{--<div class="col-md-6">
        <div class="form-field">
            <label>Quantity</label>
            <div class="form-field cardNumber">
                <input type="number" min="0" name="quantity" placeholder="Enter the quantity of the product" />
                <div class="input-group-addon">
                    <select name="unit" id="select">
                        <option value="">units</option>
                    <option value="">Kilograms</option>
                    <option value="">grams</option>
                    <option value="">tonnes</option>
                    <option value="">litres</option>
                </select>
                </div>
            </div>
        </div>
    </div>--}}
    <div class="col-md-6">
        <div class="col-md-4">
            <div class="form-field">
                <label>Quantity <span class="red">*</span></label>
                <input type="number" min="0" name="quantity[]" placeholder="Enter the quantity of the product" />
            </div>
        </div>
        <div class="col-md-2" >
            <div class="form-field form-inline">
                <label for="select" title="unit">Units</label>
                <select name="unit[]" id="select">unit
                    <option value="pieces">units</option>
                    <option value="kilogram">Kilograms</option>
                    <option value="grams">grams</option>
                    <option value="tonnes">tonnes</option>
                    <option value="litres">litres</option>
                </select>

            </div>
        </div>

    </div>
    <div class="col-md-12">
        <div class="form-field">
            <label>Images of product <span class="red">*</span></label>
            <input type="file" accept="image/*" name="media[1][]" placeholder="Upload all the images of the product" multiple/>
        </div>
    </div>

{{--end of duplicate--}}

                    <label>
                        <span class="red">Add more products</span>
                    </label><br>

                <button type="button" id="add" class="vk-btn vk-btn-m  vk-btn-default "><i class="fa fa-plus"></i></button>
    </div>
    </div>


    <div class="col-md-12" >
        <div class="form-field">
            <label for="select" title="currency">Please select the currency you have priced your products in</label>
            <select name="currency" id="currency">
                @foreach($step->customizeData() as $country)
                <option value="{{$country->currency_code}}">{{$country->name.' - '.$country->currency.' - '.$country->currency_symbol}}</option>
                @endforeach
            </select>

        </div>
    </div>



</div>





