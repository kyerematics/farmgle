<?php

namespace Farmgle;

use Farmgle\Modules\Follower;
use Farmgle\Modules\Post;
use Farmgle\Modules\Product;
use Farmgle\Modules\SocialMediaHandlers;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Farmgle\Modules\Media as Association;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * Farmgle\Manufacturer
 *
 * @property int $id
 * @property string $name
 * @property string $legal_name
 * @property string $address
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Product[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\SocialMediaHandlers[] $socialMediaHandlers
 * @property-read \Farmgle\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereLegalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $email
 * @property string|null $phone
 * @property string $city
 * @property string $state
 * @property string $country
 * @property float|null $latitude
 * @property float|null $longitude
 * @property string|null $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereProfile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Manufacturer whereState($value)
 * @property-read \Farmgle\Identity $identity
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Media[] $mediaAssociation
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Follower[] $follows
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Post[] $posts
 */
class Manufacturer extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'legal_name',
        'email',
        'phone',
        'address',
        'city',
        'state',
        'country',
        'profile',
        'latitude',
        'longitude',
    ];



    /**
     * The attributes that should manufacturer's permission.
     *
     * @var array
     */
    protected $permissions = [
        'read-blog',
        'read-profile',
        'create-comment',
        'update-comment',
        'read-comment',
        'delete-comment',
        'create-product',
        'update-product',
        'read-product',
        'delete-product',
        'buy-product',
        'sell-product',
        'read-auction',
        'bid-auction',
    ];


    /*
     * Get the institution  belonging to the user
     * */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /*Freelancer has many social media handlers
    * Get all freelancer's social media accounts
    */
    public function socialMediaHandlers(){
        return $this->morphMany(SocialMediaHandlers::class, 'sociable');
    }

    public function products()
    {
        return $this->morphMany(Product::class,'producible');
    }

    public function identity()
    {
        return $this->morphOne(Identity::class, 'identifiable');
    }

    /*
     * This returns the models this user follows
     * */
    public function follows()
    {
        return $this->morphMany(Follower::class,'followable');
    }

    /*
     * This method is responsible for storing all media for safe keep
     * */
    public function mediaAssociation()
    {
        return $this->morphMany(Association::class, 'associable');
    }

    public function registerMediaCollections()
    {

        $this
            ->addMediaCollection('listing')
            ->registerMediaConversions(function (Media $media = null){
                $this
                    ->addMediaConversion('thumb-1000')
                    ->width(1000)
                    ->height('650');

                $this
                    ->addMediaConversion('thumb-577')
                    ->width(577)
                    ->height(375);

                $this
                    ->addMediaConversion('thumb-258')
                    ->width(258)
                    ->height(167);

                $this
                    ->addMediaConversion('thumb-100')
                    ->width(100)
                    ->height(100);

                $this
                    ->addMediaConversion('thumb-200')
                    ->width(200)
                    ->height(200);
            });


        /*
         * Values here are based on guess work
         * Values needs to be recalculated
         * */
        $this
            ->addMediaCollection('slider')
            ->registerMediaConversions(function (Media $media){
                $this
                    ->addMediaConversion('slider')
                    ->width(577)
                    ->height(375);
            });


    }



    public function posts()
    {
        return $this->morphMany(Post::class,'postable');
    }
}
