<?php

namespace Farmgle;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * Farmgle\Customer
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read \Farmgle\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Customer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Customer whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Farmgle\Identity $identity
 * @property int $user_id
 */
class Customer extends Model implements HasMedia
{
    use HasMediaTrait;


    /**
     * The attributes that should customer's permission.
     *
     * @var array
     */
    protected $permissions = [
        'read-blog',
        'read-profile',
        'create-appointment',
        'update-appointment',
        'read-appointment',
        'delete-appointment',
        'create-comment',
        'update-comment',
        'read-comment',
        'delete-comment',
        'read-product',
        'buy-product',
        'read-auction',
        'bid-auction',
    ];



    /*
     * Get the institution  belonging to the user
     * */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('customer')
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion('thumb-100')
                    ->width(100)
                    ->height(100);

                $this
                    ->addMediaConversion('thumb-200')
                    ->width(200)
                    ->height(200);
            });
    }


    public function identity()
    {
        return $this->morphOne(Identity::class, 'identifiable');
    }
}
