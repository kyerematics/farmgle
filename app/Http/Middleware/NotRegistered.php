<?php

namespace Farmgle\Http\Middleware;

use Closure;
use Farmgle\User;
use Illuminate\Support\Facades\Auth;

class NotRegistered
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (count(Auth::user()->getRoleNames()) == 0 ){
            return redirect()->route('selectCriterion');
        }
            return $next($request);
    }
}
