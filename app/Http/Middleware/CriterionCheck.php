<?php

namespace Farmgle\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class CriterionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->hasAnyRole(Role::all())){
            if ($user->hasRole('customer')){
                return redirect()->home();
            }
            elseif ($user->hasRole('professional') || $user->hasRole('institution')){
            //$userType = $user->getRoleNames()->first();
            return redirect()->route('wizard.serviceOriented');
        }elseif ($user->hasRole('farmer') || $user->hasRole('manufacturer')){
                return redirect()->route('wizard.seller');
            }

                return redirect()->route('wizard.logistics');

        }

        return $next($request);
    }
}
