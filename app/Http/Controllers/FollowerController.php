<?php

namespace Farmgle\Http\Controllers;

use Farmgle\Identity;
use Farmgle\Modules\Follower;
use Farmgle\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FollowerController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $identifier
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$identifier)
    {

        $identity = Identity::whereIdentifier($identifier)->firstOrFail();

        if ((get_class($identity->identifiable) == User::class && auth()->id() == $identity->identifiable->user->id) == true){
            return redirect()->back();
        }
        else
        $follow = new Follower([
            'user_id' => auth()->id(),
        ]);

        $identity->identifiable->follows()->save($follow);
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \Farmgle\Modules\Follower  $follow
     * @return \Illuminate\Http\Response
     */
    public function show(Follower $follow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Farmgle\Modules\Follower  $follow
     * @return \Illuminate\Http\Response
     */
    public function edit(Follower $follow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Farmgle\Modules\Follower  $follow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Follower $follow)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $identifier
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($identifier)
    {
        $identity = Identity::whereIdentifier($identifier)->firstOrFail();

        if ((get_class($identity->identifiable) == User::class && auth()->id() == $identity->identifiable->user->id) == true){
            return redirect()->back();
        }
        else
            /*
             * Delete user associated information
             * */

            Auth::user()->followers()
                                    ->whereFollowableType(get_class($identity->identifiable))
                                    ->whereFollowableId($identity->identifiable->id)
                                    ->delete();
        return redirect()->back();
    }
}
