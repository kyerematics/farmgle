<?php

namespace Farmgle\Http\Controllers\Seller;

use Farmgle\Farmer;
use Farmgle\Identity;
use Farmgle\Manufacturer;
use Farmgle\Modules\Product;
use Illuminate\Http\Request;
use Farmgle\Http\Controllers\Controller;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $farmers = Farmer::all();
        //$manufacturers = Manufacturer::all();
        //$allSellers = [$farmers, $manufacturers];

        $products = Product::with('media')->get();
        return view('market.all_products',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /*
     * Retrieve page for the farmer or manufacturer
     * */
    public function shop($seller)
    {
        $identity = Identity::whereIdentifier($seller)->firstOrFail();
        $identifiable = $identity->identifiable->with([
            'media','products'=>function($query){
            $query->with('media');
                }])->firstOrFail();


        if (get_class($identifiable) == Farmer::class){
            return view('market.seller.farmer',compact('identifiable'));
        }
        return view('market.seller.seller',compact('identifiable'));

    }
    /**
     * Display the specified resource.
     *
     * @param  $product
     * @return \Illuminate\Http\Response
     */
    public function showProduct($seller,$product)
    {
        /*$identity = Identity::whereIdentifier($seller)->firstOrFail();
        $identifiable = $identity->identifiable->firstOrFail();
        */

        $item = Product::whereUuid($product)
            //->orWhere('id',$product) //For development purposes
            ->with('media')->firstOrFail();

        return view('market.single_product',compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
