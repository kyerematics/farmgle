<?php

namespace Farmgle\Http\Controllers\Registration;

use Farmgle\Modules\Registration\Customer;
use Illuminate\Http\Request;
use Farmgle\Http\Controllers\Controller;
use Smajti1\Laravel\Exceptions\StepNotFoundException;
use Smajti1\Laravel\Wizard;

/**
 * Class CustomerController
 * @package Farmgle\Http\Controllers\Registration
 */
class CustomerController extends Controller
{
    public $steps = [
        'set-username-key' => Customer::class,


    ];

    protected $wizard;

    public function __construct()
    {
        $this->wizard = new Wizard($this->steps, $sessionKeyName = 'user');
    }

    public function wizard($step = null)
    {
        try {
            if (is_null($step)) {
                $step = $this->wizard->firstOrLastProcessed();
            } else {
                $step = $this->wizard->getBySlug($step);
            }
        } catch (StepNotFoundException $e) {
            abort(404);
        }

        return view('wizard.user.base', compact('step'));
    }

    public function wizardPost(Request $request, $step = null)
    {
        try {
            $step = $this->wizard->getBySlug($step);
        } catch (StepNotFoundException $e) {
            abort(404);
        }

        $this->validate($request, $step->rules($request));
        $step->process($request);

        return redirect()->route('wizard.user', [$this->wizard->nextSlug()]);
    }
}
