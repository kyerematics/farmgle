<?php
/**
 * Created by PhpStorm.
 * User: kyerematics
 * Date: 5/4/19
 * Time: 7:19 AM
 */

namespace Farmgle\Http\Controllers\Registration;

use Farmgle\Modules\Registration\ServiceOriented\AddProfile;
use Farmgle\Modules\Registration\ServiceOriented\AddService;
use Farmgle\Modules\Registration\ServiceOriented\TermsAndConditions;
use Farmgle\Modules\Registration\ServiceOriented\VisionMissionStatement;
use Illuminate\Http\Request;
use Farmgle\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Smajti1\Laravel\Exceptions\StepNotFoundException;
use Smajti1\Laravel\Wizard;


class ServiceOrientedController extends Controller
{


    public $steps = [
        'serviceOriented' =>  TermsAndConditions::class,
                                AddProfile::class,
                                VisionMissionStatement::class,
                                AddService::class

    ];

    protected $wizard;


    public function __construct()
    {
        $this->middleware(['auth']);
        $this->wizard = new Wizard($this->steps, $sessionKeyName = 'user');
    }

    public function wizard($step = null)
    {
        try {
            if (is_null($step)) {
                $step = $this->wizard->firstOrLastProcessed();
            } else {
                $step = $this->wizard->getBySlug($step);
            }
        } catch (StepNotFoundException $e) {
            abort(404);
        }

        return view('registration.serviceOriented.base', compact('step'));
    }

    public function wizardPost(Request $request, $step = null)
    {
        try {
            $step = $this->wizard->getBySlug($step);
        } catch (StepNotFoundException $e) {
            abort(404);
        }


        Validator::make($request->all(),$step->rules($request))->validate();
        $step->process($request);

        if ($step->number == $this->wizard->limit() ?? true){
            return redirect()->home();
        }
        return redirect()->route('wizard.serviceOriented', [$this->wizard->nextSlug()]);
    }


}
