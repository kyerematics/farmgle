<?php

namespace Farmgle;

use Farmgle\Modules\Follower;
use Farmgle\Modules\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Models\Permission;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

/**
 * Farmgle\User
 *
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $phone
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Farmer[] $farmers
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Freelancer[] $freelancers
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Institution[] $institutions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Manufacturer[] $manufacturers
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Customer[] $customers
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Logistics[] $logistics
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read \Farmgle\User $registrar
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Follower[] $followers
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Follower[] $follows
 * @property mixed $first_name
 * @property-read mixed $full_name
 * @property mixed $last_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Post[] $posts
 */
class User extends Authenticatable implements MustVerifyEmail, HasMedia
{
    use Notifiable, HasRoles, HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName','lastName', 'email', 'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * The attributes that should farmer's permission.
     *
     * @var array
     */
    protected $farmerPermissions = [
        'read-blog',
        'read-profile',
        'create-appointment',
        'update-appointment',
        'read-appointment',
        'delete-appointment',
        'create-comment',
        'update-comment',
        'read-comment',
        'delete-comment',
        'create-product',
        'update-product',
        'read-product',
        'delete-product',
        'buy-product',
        'sell-product',
        'auction-service',
        'auction-product',
        'create-auction',
        'update-auction',
        'delete-auction',
        'read-auction',
        'bid-auction'
    ];

    /**
     * The attributes that should manufacturer's permission.
     *
     * @var array
     */
    protected $manufacturerPermissions = [
        'read-blog',
        'read-profile',
        'create-comment',
        'update-comment',
        'read-comment',
        'delete-comment',
        'create-product',
        'update-product',
        'read-product',
        'delete-product',
        'buy-product',
        'sell-product',
        'read-auction',
        'bid-auction',
    ];

    /**
     * The attributes that should freelancer's permission.
     *
     * @var array
     */
    protected $freelancerPermissions = [
        'create-blog',
        'update-blog',
        'delete-blog',
        'read-blog',
        'create-profile',
        'update-profile',
        'read-profile',
        'delete-profile',
        'create-comment',
        'update-comment',
        'read-comment',
        'delete-comment',
        'read-product',
        'buy-product',
        'read-auction',
        'bid-auction',
    ];

    /**
     * The attributes that should institution's permission.
     *
     * @var array
     */

    protected $institutionPermissions = [
        'create-blog',
        'update-blog',
        'delete-blog',
        'read-blog',
        'create-profile',
        'update-profile',
        'read-profile',
        'delete-profile',
        'create-comment',
        'update-comment',
        'read-comment',
        'delete-comment',
        'read-product',
        'buy-product',
        'read-auction',
        'bid-auction',
    ];

    /**
     * The attributes that should customer's permission.
     *
     * @var array
     */
    protected $customerPermissions = [
        'read-blog',
        'read-profile',
        'create-appointment',
        'update-appointment',
        'read-appointment',
        'delete-appointment',
        'create-comment',
        'update-comment',
        'read-comment',
        'delete-comment',
        'read-product',
        'buy-product',
        'read-auction',
        'bid-auction',
    ];

    /**
     * The attributes that should logistics' permission.
     *
     * @var array
     */
    protected $logisticsPermissions = [
        'create-comment',
        'update-comment',
        'read-comment',
        'delete-comment',
        'read-product',
        'buy-product',
        'read-auction',
        'bid-auction',
    ];


    /*
     * Get the institution associated with this user
     * */
    public function institutions()
    {
        return $this->hasMany(Institution::class);
    }


    /*
     * Get the freelancer associated with this user
     *
     * */
    public function freelancers()
    {
        return $this->hasMany(Freelancer::class);
    }

    /*
     *  Get the farmer associated with this user
     * */
    public function farmers()
    {
        return $this->hasMany(Farmer::class);
    }

    /*
     *  Get the manufacturers associated with this user
     * */
    public function manufacturers()
    {
        return $this->hasMany(Manufacturer::class);
    }

    /*
     *  Get the Delivery Services associated with this user
     * */
    public function logistics()
    {
        return $this->hasMany(Logistics::class);
    }

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }


    public function registrar()
    {
        return $this->hasOne(User::class);
    }


    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function posters()
    {
        return $this->morphMany(Post::class,'postable');
    }

    /*
     * This returns users' followers
     * */
    public function followers()
    {
        return $this->hasMany(Follower::class);
    }

    /*
     * This returns the models this user follows
     * */
    public function follows()
    {
        return $this->morphMany(Follower::class,'followable');
    }


    public function isFollowing($value)
    {


        $identity =Identity::whereIdentifier($value)->firstOrFail();

        $getFollowers= Follower::whereUserId($this->id)
                                ->whereFollowableType(get_class($identity->identifiable))
                                ->whereFollowableId($identity->identifiable->id)
                                ->count();

        if ($getFollowers === 0){

            return false;
        }
        return true;
    }

    public function getFollowers()
    {
        $followers = $this->followers()->get();
        return $followers;
    }

    public function createFarmerPermissions($role)
    {
        $array1 = $this->farmerPermissions;
        foreach($array1 as $arr){
            $permission = Permission::firstOrCreate([
                'name' => $arr
            ]);
            $permission->assignRole($role);

        }
    }

    public function createLogisticsPermissions($role)
    {
        $array = $this->logisticsPermissions;
        foreach ($array as $item) {
            $permission = Permission::firstOrCreate([
                'name'  => $item
            ]);
            $permission->assignRole($role);
        }
    }


    public function createManufacturerPermissions($role)
    {
        $array2 = $this->manufacturerPermissions;
        foreach($array2 as $arr){
            $permission = Permission::firstOrCreate([
                'name' => $arr
            ]);
            $permission->assignRole($role);
        }
    }

    public function createInstitutionPermissions($role)
    {
        $array4 = $this->institutionPermissions;
        foreach($array4 as $arr){
            $permission = Permission::firstOrCreate([
                'name' => $arr
            ]);
            $permission->assignRole($role);
        }

    }

    public function createCustomerPermissions($role)
    {
        $array6 = $this->customerPermissions;
        foreach($array6 as $arr){
            $permission = Permission::firstOrCreate([
                'name' => $arr
            ]);
            $permission->assignRole($role);
        }
    }


    public function createFreelancerPermissions($role)
    {
        $array3 = $this->freelancerPermissions;
        foreach($array3 as $arr){
            $permission = Permission::firstOrCreate([
                'name' => $arr
            ]);
            $permission->assignRole($role);
        };

    }

    public function revokeFarmerPermissions()
    {
        $array1 = $this->farmerPermissions;
        if (Auth::user()->hasRole('farmer')){
            $role = Auth::user()->removeRole('farmer')->revokePermissionTo($array1);

        }

    }

    public function revokeLogisticsPermissions()
    {
        $array1 = $this->logisticsPermissions;
        if (Auth::user()->hasRole('logistics')){
            $role = Auth::user()->removeRole('logistics')->revokePermissionTo($array1);
        }

    }


public function revokeManufacturerPermissions()
    {
        $array1 = $this->manufacturerPermissions;
        if (Auth::user()->hasRole('manufacturer')){
            $role = Auth::user()->removeRole('manufacturer')->revokePermissionTo($array1);
        }

    }

    public function userType()
    {
        //$user = Auth::user();
        if ($this->hasRole('professional') == true){
             $userType = Freelancer::whereUserId($this->id)->firstOrFail();

        }
        elseif ($this->hasRole('farmer') == true){
             $userType = Farmer::whereUserId($this->id)->firstOrFail();
        }
        elseif ($this->hasRole('manufacturer') == true){
             $userType = Manufacturer::whereUserId($this->id)->firstOrFail();
        }
        elseif ($this->hasRole('institution') == true){
             $userType = Institution::whereUserId($this->id)->firstOrFail();
        }
        elseif ($this->hasRole('logistics') == true){
             $userType = Logistics::whereUserId($this->id)->firstOrFail();
        }else {
            $userType = Customer::whereUserId($this->id)->firstOrFail();
        }
        return $userType;
    }


    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('avatar')
            ->singleFile()
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion('thumb-100')
                    ->width(100)
                    ->height(100);

                $this
                    ->addMediaConversion('thumb-200')
                    ->width(200)
                    ->height(200);
            });
    }

    public function getFullNameAttribute()
    {
        return "{$this->firstName} {$this->lastName}";
    }

    public function getFirstNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getLastNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function setFirstNameAttribute($value)
    {
        $this->attributes['firstName'] = mb_strtolower($value);
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['lastName'] = mb_strtolower($value);
    }

    /*
     * Retrieve this user's permissions names via his/her roles
     * */

    public function getPermissionNamesViaRoles()
    {
        return $this->getPermissionsViaRoles()->pluck('name');
    }



}
