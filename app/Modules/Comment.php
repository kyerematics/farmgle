<?php

namespace Farmgle\Modules;

use Illuminate\Database\Eloquent\Model;

/**
 * Farmgle\Modules\Comment
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Comment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Comment extends Model
{
    //
}
