<?php
/**
 * Created by PhpStorm.
 * User: kyerematics
 * Date: 5/8/19
 * Time: 5:56 PM
 */

namespace Farmgle\Modules\Registration\Seller;


use Illuminate\Http\Request;
use Smajti1\Laravel\Step;

class AddPayment extends Step
{
    public static $label = 'AddPayment';
    public static $slug = 'add_payment';
    public static $view = 'registration.seller.2';

    public function process(Request $request)
    {
        // for example, create user

        // next if you want save one step progress to session use
        $this->saveProgress($request);
    }

    public function rules(Request $request = null): array
    {
        return [

        ];
    }
}
