<?php
/**
 * Created by PhpStorm.
 * User: kyerematics
 * Date: 5/2/19
 * Time: 2:32 PM
 */

namespace Farmgle\Modules\Registration\Seller;

use Smajti1\Laravel\Step;
use Illuminate\Http\Request;

class TermsAndConditions extends Step
{

    public static $label = 'Terms And Conditions';
    public static $slug = 'terms_n_conditions';
    public static $view = 'registration.seller.0';

    public function process(Request $request)
    {
        // for example, create user

        // next if you want save one step progress to session use
        $this->saveProgress($request);
    }

    public function rules(Request $request = null): array
    {
        return [
            'terms' => 'required|accepted',
        ];
    }
}
