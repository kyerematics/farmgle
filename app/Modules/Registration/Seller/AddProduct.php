<?php
/**
 * Created by PhpStorm.
 * User: kyerematics
 * Date: 5/8/19
 * Time: 5:57 PM
 */

namespace Farmgle\Modules\Registration\Seller;


use Farmgle\Modules\Country;
use Farmgle\Modules\Media;
use Farmgle\Modules\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Smajti1\Laravel\Step;

class AddProduct extends Step
{
    public static $label = 'AddProduct';
    public static $slug = 'add_product';
    public static $view = 'registration.seller.3';

    public function process(Request $request)
    {
        $user = Auth::user();

        $files = [];
        //$products = [];
        $items = $request['product'];
        $medias =$request->file(['media']);


        foreach ($items as $key => $item) {
            $products = new Product([
                'product'           => $item,
                'description'       => $request['description'][$key],
                'price'             => $request['price'][$key],
                'currency'          => $request['currency'],
                'quantity'          => $request['quantity'][$key],
                'unit_of_measurement'=>$request['unit'][$key],
                'uuid'              => Str::slug(Str::orderedUuid(),''),
            ]);

            foreach ($medias[$key] as $media) {
               array_push($files, new Media(['path' => $media->store('products','public')]));
            }

            $mediaAssociation = $user->userType()->products()->save($products);
            $mediaAssociation->mediaAssociation()->saveMany($files);
            foreach ($files as $file) {
                $mediaAssociation->addMedia('storage/'.$file['path'])
                    ->toMediaCollection('product');
            }
        }





        /*
         * Images of product
         * */

        // next if you want save one step progress to session use
        $this->saveProgress($request);
    }

    public function rules(Request $request = null): array
    {
        return [
            'product.*'       => 'required|string',
            'description.*'   => 'required|string',
            'price.*'         => 'required|numeric',
            'quantity.*'      => 'required|numeric',
            'currency'        => 'required|string',
            'unit_of_measurement.*'=> 'required|string',
            'media.*.*' => 'required|image|mimes:jpeg,bmp,png,jpg',
        ];
    }

    public function customizeData()
    {
       return $country = Country::all();
    }


    public function saveProgress(Request $request, array $additionalData = [])
    {
        $wizardData = $this->wizard->data();
        $wizardData[$this::$slug] = $request->except('step', '_token','media');
        $wizardData = array_merge($wizardData, $additionalData);

        $this->wizard->data($wizardData);
    }

}
