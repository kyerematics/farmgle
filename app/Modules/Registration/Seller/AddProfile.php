<?php
/**
 * Created by PhpStorm.
 * User: kyerematics
 * Date: 5/8/19
 * Time: 5:43 PM
 */

namespace Farmgle\Modules\Registration\Seller;


use Farmgle\Farmer;
use Farmgle\Identity;
use Farmgle\Manufacturer;
use Farmgle\Modules\SocialMediaHandlers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Smajti1\Laravel\Step;

class AddProfile extends Step
{
    public static $label = 'AddProfile';
    public static $slug = 'add_profile';
    public static $view = 'registration.seller.1';

    public function process(Request $request)
    {
        $user = Auth::user();


        if ($user->hasRole('manufacturer')){
            $manufacturer = new Manufacturer([
                'name'          => $request['name'],
                'legal_name'    => $request['legal_name'],
                'email'         =>$request['email'],
                'phone'         =>$request['phone'],
                'address'       =>$request['address'],
                'city'          =>$request['city'],
                'state'         =>$request['state'],
                'country'       =>$request['country'],
                'profile'       => $request['description']
            ]);

            $userType = $user->manufacturers()->save($manufacturer);


        }else{
            $farmer = new Farmer([
                'legal_name'    => $request['legal_name'],
                'email'         => $request['email'],
                'phone'         => $request['phone'],
                'address'       => $request['address'],
                'city'          => $request['city' ],
                'state'         => $request['state'],
                'country'       => $request['country'],
                'profile'       => $request['description']
            ]);
            $userType = $user->farmers()->save($farmer);
        }

        $identity = new Identity([
            'identifier'    => str_slug(Str::orderedUuid(),''),
            //'slug'          => str_slug($request['legal_name']),
        ]);
        $userType->identity()->save($identity);



        if ($request->anyFilled(['twitter','facebook','linkedin','whatsapp','instagram','website'])){

            /*if ($user->hasRole('professional')){
                $userType = Freelancer::findOrFail($user)->first();

            }
            else{
                $userType = Institution::findOrFail($user)->first();
            }*/

            if ($request->filled('facebook')) {
                $socialMedia = new SocialMediaHandlers([
                    'name' => 'facebook',
                    'uri' => $request['facebook']
                ]);
                $userType->socialMediaHandlers()->save($socialMedia);
            }

            if ($request->filled('twitter')) {
                $socialMedia = new SocialMediaHandlers([
                    'name' => 'twitter',
                    'uri' => $request['twitter']
                ]);
                $userType->socialMediaHandlers()->save($socialMedia);
            }

            if ($request->filled('linkedin')) {
                $socialMedia = new SocialMediaHandlers([
                    'name' => 'linkedin',
                    'uri' => $request['linkedin']
                ]);
                $userType->socialMediaHandlers()->save($socialMedia);
            }

            if ($request->filled('whatsapp')) {
                $socialMedia = new SocialMediaHandlers([
                    'name' => 'whatsapp',
                    'uri' => $request['whatsapp']
                ]);
                $userType->socialMediaHandlers()->save($socialMedia);
            }

            if ($request->filled('instagram')) {
                $socialMedia = new SocialMediaHandlers([
                    'name' => 'instagram',
                    'uri' => $request['instagram']
                ]);
                $userType->socialMediaHandlers()->save($socialMedia);
            }

            if ($request->filled('website')) {
                $socialMedia = new SocialMediaHandlers([
                    'name' => 'website',
                    'uri' => $request['website']
                ]);
                $userType->socialMediaHandlers()->save($socialMedia);
            }


        }

        $this->saveProgress($request);
    }

    public function rules(Request $request = null): array
    {
        if (Auth::user()->hasRole('manufacturer')){
            return [
                'legal_name' => 'required|string',
                'email'      => 'nullable|email|unique:manufacturers',
                'phone'      => 'nullable|numeric|unique:manufacturers',
                'address'    => 'required|string',
                'city'       => 'required|string',
                'state'      => 'required|string',
                'country'    => 'required|string',
                'description'=> 'nullable|string',
                'twitter'    => 'nullable|url',
                'facebook'   => 'nullable|url',
                'linkedin'   => 'nullable|url',
                'instagram'   => 'nullable|url',
                'website'   => 'nullable|url',
                'whatsapp'   => 'nullable|numeric'
            ];
        }
        return [
            //Farmer rules,
            'legal_name' => 'nullable|string',
            'email'      => 'nullable|email|unique:farmers',
            'phone'      => 'nullable|numeric|unique:farmers',
            'address'    => 'required|string',
            'city'       => 'required|string',
            'state'      => 'required|string',
            'country'    => 'required|string',
            'description'=> 'nullable|string',
            'twitter'    => 'nullable|url',
            'facebook'   => 'nullable|url',
            'linkedin'   => 'nullable|url',
            'website'   => 'nullable|url',
            'whatsapp'   => 'nullable|numeric'
        ];
    }
}
