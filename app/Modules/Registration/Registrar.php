<?php

namespace Farmgle\Modules\Registration;

use Farmgle\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Farmgle\Modules\Registration\Registrar
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Farmgle\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Registration\Registrar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Registration\Registrar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Registration\Registrar query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Registration\Registrar whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Registration\Registrar whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Registration\Registrar whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Registrar extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
