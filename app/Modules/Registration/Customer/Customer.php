<?php
/**
 * Created by PhpStorm.
 * User: kyerematics
 * Date: 5/2/19
 * Time: 2:32 PM
 */

namespace Farmgle\Modules\Registration\Customer;

use Smajti1\Laravel\Step;
use Illuminate\Http\Request;

class Customer extends Step
{

    public static $label = 'Set user name';
    public static $slug = 'set-user-name';
    public static $view = 'wizard.user.steps._set_user_name';

    public function process(Request $request)
    {
        // for example, create user

        // next if you want save one step progress to session use
        $this->saveProgress($request);
    }

    public function rules(Request $request = null): array
    {
        return [
            'username' => 'required|min:4|max:255',
        ];
    }
}
