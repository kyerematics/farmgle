<?php
/**
 * Created by PhpStorm.
 * User: kyerematics
 * Date: 5/2/19
 * Time: 2:31 PM
 */

namespace Farmgle\Modules\Registration\ServiceOriented;

use Farmgle\Freelancer;
use Farmgle\Institution;
use Farmgle\Modules\Media;
use Farmgle\Modules\Service;
use Illuminate\Support\Facades\Auth;
use Smajti1\Laravel\Step;
use Illuminate\Http\Request;


class AddService extends Step
{

    public static $label = 'AddService';
    public static $slug = 'add_service';
    /**
     * @$view resources/views
     * @var string
     */
    public static $view = 'registration.serviceOriented.3';

    public function process(Request $request)
    {

        $user =Auth::user();
        $services =$request['title'];

        $arr1 =[];
        foreach ($services as $key => $service) {
            array_push($arr1, new Service([
                'title'     => $service,
                'description'=> $request['description'][$key]
            ]));

        }
        $user->userType()->services()->saveMany($arr1);



        /*
         * Save images
         * */
        $files = [];
        $folderName =$user->getRoleNames()->first();

        if ($request->hasFile('cover_image')){
            $cover_image = new Media([
                'path' => $request->file('cover_image')->store($folderName,'public'),
            ]);

            $user->userType()->mediaAssociation()->save($cover_image);

            $user->userType()
                ->addMedia('storage/'.$cover_image['path'])
                ->preservingOriginal()
                ->toMediaCollection('listing');
            $user->userType()
                ->addMedia('storage/'.$cover_image['path'])
                ->preservingOriginal()
                ->toMediaCollection('cover-page');
        }

        if ($request->hasFile('media')){

            $media = $request->file(['media']);
            foreach ($media as $item) {

                array_push($files,new Media([
                    'path'      => $item->store($folderName,'public'),
                ]));
            }
            $user->userType()->mediaAssociation()->saveMany($files);

            foreach ($files as $file) {
                $user->userType()->addMedia('storage/'.$file['path'])
                    ->toMediaCollection('services');
            }

        }


        $this->saveProgress($request);
    }

        public function rules(Request $request = null): array
    {
        return [
            'title.*'         => 'required|string',
            'description.*'   => 'required|string',
            'cover_image' => 'nullable|image|mimes:jpeg,bmp,png,jpg',
            'media.*' => 'nullable|image|mimes:jpeg,bmp,png,jpg',
        ];
    }


    public function customizeData()
    {

    }

    public function saveProgress(Request $request, array $additionalData = [])
    {
        $wizardData = $this->wizard->data();
        $wizardData[$this::$slug] = $request->except('step', '_token','cover_image','media');
        $wizardData = array_merge($wizardData, $additionalData);

        $this->wizard->data($wizardData);
    }

}

