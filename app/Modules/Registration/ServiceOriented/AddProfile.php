<?php
/**
 * Created by PhpStorm.
 * User: kyerematics
 * Date: 5/2/19
 * Time: 2:31 PM
 */

namespace Farmgle\Modules\Registration\ServiceOriented;

use Farmgle\Freelancer;
use Farmgle\Identity;
use Farmgle\Institution;
use Farmgle\Modules\Country;
use Farmgle\Modules\SocialMediaHandlers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Smajti1\Laravel\Step;
use Illuminate\Http\Request;

/**
 * @mixin  \Eloquent
 * */
class AddProfile extends Step
{

    public static $label = 'AddProfile';
    public static $slug = 'add_profile';
    /**
     * @$view resources/views
     * @var string
     */
    public static $view = 'registration.serviceOriented.1';

    public function process(Request $request)
    {
        $user = Auth::user();


        if ($user->hasRole('professional')){
            $freelancer = new Freelancer([
            'profession'      =>$request['profession'],
            'email'         =>$request['email'],
            'phone'         =>$request['phone'],
            'address'       =>$request['address'],
            'city'      =>$request['city'],
            'state'         =>$request['state'],
            'country'       =>$request['state'],
            ]);

            $userType = $user->freelancers()->save($freelancer);


        }else{
            $institution = new Institution([
                'name'      => $request['institution'],
                'email'         => $request['email'],
                'phone'         => $request['phone'],
                'address'       => $request['address'],
                'city'      => $request['city' ],
                'state'         => $request['state'],
                'country'       => $request['country'],
            ]);
            $userType = $user->institutions()->save($institution);
        }


        $identity = new Identity([
            'identifier'    => Str::slug(Str::orderedUuid(),''),
            //'slug'          => str_slug($request['name'] || $request['profession']),
        ]);
        $userType->identity()->save($identity);

        if ($request->anyFilled(['twitter','facebook','linkedin','whatsapp','instagram'])){

            /*if ($user->hasRole('professional')){
                $userType = Freelancer::findOrFail($user)->first();

            }
            else{
                $userType = Institution::findOrFail($user)->first();
            }*/

            if ($request->filled('facebook')) {
                $socialMedia = new SocialMediaHandlers([
                    'name' => 'facebook',
                    'uri' => $request['facebook']
                ]);
                $userType->socialMediaHandlers()->save($socialMedia);
            }

            if ($request->filled('twitter')) {
                $socialMedia = new SocialMediaHandlers([
                    'name' => 'twitter',
                    'uri' => $request['twitter']
                ]);
                $userType->socialMediaHandlers()->save($socialMedia);
            }

            if ($request->filled('linkedin')) {
                $socialMedia = new SocialMediaHandlers([
                    'name' => 'linkedin',
                    'uri' => $request['linkedin']
                ]);
                $userType->socialMediaHandlers()->save($socialMedia);
            }

            if ($request->filled('whatsapp')) {
                $socialMedia = new SocialMediaHandlers([
                    'name' => 'whatsapp',
                    'uri' => $request['whatsapp']
                ]);
                $userType->socialMediaHandlers()->save($socialMedia);
            }

            if ($request->filled('instagram')) {
                $socialMedia = new SocialMediaHandlers([
                    'name' => 'instagram',
                    'uri' => $request['instagram']
                ]);
                $userType->socialMediaHandlers()->save($socialMedia);
            }

            if ($request->filled('website')) {
                $socialMedia = new SocialMediaHandlers([
                    'name' => 'website',
                    'uri' => $request['website']
                ]);
                $userType->socialMediaHandlers()->save($socialMedia);
            }




        }

        $this->saveProgress($request);
    }

        public function rules(Request $request = null): array
    {
        if (Auth::user()->hasRole('professional')){
            return [
                'profession' => 'required|string',
                'email'      => 'nullable|email|unique:freelancers',
                'phone'      => 'nullable|numeric|unique:freelancers',
                'address'    => 'required|string',
                'city'       => 'required|string',
                'state'      => 'required|string',
                'country'    => 'required|string',
                'twitter'    => 'nullable|url',
                'facebook'   => 'nullable|url',
                'linkedin'   => 'nullable|url',
                'website'   => 'nullable|url',
                'whatsapp'   => 'nullable|numeric'
            ];
        }
        return [
            //Institution rules,
            'institution' => 'required|string',
            'email'      => 'nullable|email|unique:institutions',
            'phone'      => 'nullable|numeric|unique:institutions',
            'address'    => 'required|string',
            'city'       => 'required|string',
            'state'      => 'required|string',
            'country'    => 'required|string',
            'twitter'    => 'nullable|url',
            'facebook'   => 'nullable|url',
            'linkedin'   => 'nullable|url',
            'website'   => 'nullable|url',
            'whatsapp'   => 'nullable|numeric'
        ];
    }

    public function customizeData()
    {
        $country = Country::all();
        return $country;
    }
}

