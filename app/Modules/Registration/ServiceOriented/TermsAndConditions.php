<?php
/**
 * Created by PhpStorm.
 * User: kyerematics
 * Date: 5/2/19
 * Time: 2:31 PM
 */

namespace Farmgle\Modules\Registration\ServiceOriented;

use Smajti1\Laravel\Step;
use Illuminate\Http\Request;


class TermsAndConditions extends Step
{

    public static $label = 'Terms and Conditions';
    public static $slug = 'terms_n_conditions';
    /**
     * @$view resources/views
     * @var string
     */
    public static $view = 'registration.serviceOriented.0';

    public function process(Request $request)
    {



        $this->saveProgress($request);
    }

        public function rules(Request $request = null): array
    {
        return [
            'terms' => 'required|accepted',
        ];
    }



    public function customizeData()
    {

    }
}

