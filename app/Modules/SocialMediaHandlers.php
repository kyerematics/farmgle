<?php

namespace Farmgle\Modules;

use Farmgle\Freelancer;
use Illuminate\Database\Eloquent\Model;

/**
 * Farmgle\Modules\SocialMediaHandlers
 *
 * @property int $id
 * @property string $sociable_type
 * @property int $sociable_id
 * @property string $name
 * @property string $uri
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $sociable
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\SocialMediaHandlers newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\SocialMediaHandlers newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\SocialMediaHandlers query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\SocialMediaHandlers whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\SocialMediaHandlers whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\SocialMediaHandlers whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\SocialMediaHandlers whereSociableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\SocialMediaHandlers whereSociableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\SocialMediaHandlers whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\SocialMediaHandlers whereUri($value)
 * @mixin \Eloquent
 */
class SocialMediaHandlers extends Model
{

    protected $fillable = [
        'name',
        'uri',
    ];
    /*Social media links belongs to a freelancer*/
    public function sociable()
    {
        return $this->morphTo();
    }
}
