<?php

namespace Farmgle\Modules;

use Illuminate\Database\Eloquent\Model;

/**
 * Farmgle\Modules\Media
 *
 * @property int $id
 * @property string $associable_type
 * @property int $associable_id
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $associable
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Media newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Media newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Media query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Media whereAssociableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Media whereAssociableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Media whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Media whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Media wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Media whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Media extends Model
{
    /*
     * This media model is responsible for storing all uploaded files for safe keep
     *
     * */
    protected $table = 'media_association';

    protected $fillable = [
            'path'
    ];

    public function associable()
    {
        return $this->morphTo();
    }
}
