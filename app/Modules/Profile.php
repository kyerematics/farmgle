<?php

namespace Farmgle\Modules;

use Farmgle\Freelancer;
use Illuminate\Database\Eloquent\Model;

/**
 * Farmgle\Modules\Profile
 *
 * @property int $id
 * @property int $profileable_id
 * @property string $profileable_type
 * @property string $who_you_are
 * @property string $mission
 * @property string $vision
 * @property string $your_why
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $profileable
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Profile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Profile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Profile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Profile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Profile whereMission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Profile whereProfileableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Profile whereProfileableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Profile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Profile whereVision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Profile whereWhoYouAre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Profile whereYourWhy($value)
 * @mixin \Eloquent
 */
class Profile extends Model
{
    protected $fillable = [
        'who_you_are',
        'mission',
        'vision',
        'your_why',
        //'profileable_id',
        //'profileable_type'
    ];
    public function profileable()
    {
        return $this->morphTo();
    }
}
