<?php

namespace Farmgle\Modules;

use Farmgle\Modules\Media as Association;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Swap\Laravel\Facades\Swap as Forex;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Torann\GeoIP\Facades\GeoIP;

/**
 * Farmgle\Modules\Product
 *
 * @property int $id
 * @property string $producible_type
 * @property int $producible_id
 * @property string $product
 * @property string $description
 * @property float $price
 * @property int $quantity
 * @property string $uuid
 * @property string|null $unit_of_measurement
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $producible
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product whereProducibleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product whereProducibleType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product whereProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product whereUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product whereUnitOfMeasurement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $currency
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Product whereCurrency($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Media[] $mediaAssociation
 */
class Product extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'product',
        'description',
        'price',
        'quantity',
        'unit_of_measurement',
        'currency',
        'uuid',
    ];


    public function producible()
    {
        return $this->morphTo();
    }


    /*
     * This method is responsible for storing all media for safe keep
     * */
    public function mediaAssociation()
    {
        return $this->morphMany(Association::class, 'associable');
    }


    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('product')
            ->registerMediaConversions(function (Media $media = null){
                $this
                    ->addMediaConversion('grid')
                    ->width(388)
                    ->height(388);

                $this
                    ->addMediaConversion('single')
                    ->width(486)
                    ->height(437);

                $this
                    ->addMediaConversion('cart')
                    ->width(78)
                    ->height(70);
            });
    }

    /*
     * Retrieve the local user's currency symbol
     * */
    public function getCurrencyLocale()
    {
        /*
         * Get the user's ip information
         * Retrieve the user's local currency via ip address
         *
         * */
        $clientCurrency = $this->clientInfo()->currency;

        if (!isset($clientCurrency)){
            $cu = $this->clientInfo()->iso_code ?? $this->clientInfo()->country;

           return $clientCurrency = Country::getCurrencyCode($cu);
        }

        return $clientCurrency;
    }

    /*
     * Get the currency pair
     *
     * */
    public function currencyPair()
    {
        $baseCurrency = $this->currency;
        $quoteCurrency = $this->getCurrencyLocale();
        $instrument = $baseCurrency. ' '.$quoteCurrency;

        return mb_strtoupper(Str::slug($instrument,'/'));

    }

    /*
     * Get the currency rate
     * */
    public function currencyRate()
    {
        $rate      = Cache::get($this->currencyPair());
        if (! Cache::has($this->currencyPair())){

                $rate  = Forex::latest($this->currencyPair())->getValue();
                Cache::put($this->currencyPair(),$rate,now()->addDay());
                return $rate;

            /*if (!isset($rate)){
                $rate  = Forex::historical($this->currencyPair(),Carbon::yesterday())->getValue();
                Cache::put($this->currencyPair(),$rate,now()->addDay());
                return $rate;
            }
            else{
                $rate   = Forex::latest($this->currencyPair())->getValue();
                Cache::put($this->currencyPair(),$rate,now()->addDay());
                return $rate;
            }*/
        }

        return $rate;
    }


    /*
     * Compute the local price for the current user;
     * */
    public function getLocalPrice()
    {
        return
            $this->getCurrencyLocale().' '
            .$this->getCurrencySymbol().' '
            .round($this->currencyRate() * $this->price,
                $this->getCurrencyDecimal());
    }



    public function getOldPrice()
    {
        return $this->getCurrencyLocale().' '.$this->price;
    }



    public function getCurrencySymbol()
    {
       $currencySymbol = Country::getCurrencySymbol($this->clientInfo()->iso_code);

       return $currencySymbol;

    }



    public function getCurrencyDecimal()
    {
        $currencyDecimal = Country::getDetails($this->clientInfo()->iso_code)
                                    ->pluck('currency_decimals')->pop();
        return (int) $currencyDecimal ?? 2;
    }


    public function clientInfo()
    {
        $ip = geoip()->getClientIP();
        //$ip ='41.66.203.108'; //ip address in Ghana
         $clientLocation = geoip()->getLocation($ip);
         return $clientLocation;
    }
}
