<?php

namespace Farmgle\Modules;

use Illuminate\Database\Eloquent\Model;

/**
 * Farmgle\Modules\Country
 *
 * @property int $id
 * @property string|null $capital
 * @property string|null $citizenship
 * @property string $country_code
 * @property string|null $currency
 * @property string|null $currency_code
 * @property string|null $currency_sub_unit
 * @property string|null $currency_symbol
 * @property int|null $currency_decimals
 * @property string|null $full_name
 * @property string $iso_3166_2
 * @property string $iso_3166_3
 * @property string $name
 * @property string $region_code
 * @property string $sub_region_code
 * @property int $eea
 * @property string|null $calling_code
 * @property string|null $flag
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereCallingCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereCapital($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereCitizenship($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereCurrencyCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereCurrencyDecimals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereCurrencySubUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereCurrencySymbol($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereEea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereIso31662($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereIso31663($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereRegionCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Country whereSubRegionCode($value)
 * @mixin \Eloquent
 */
class Country extends Model
{
    public static function getCurrencyCode($value)
    {
        return self::where('iso_3166_2',$value)
            ->orWhere('iso_3166_3',$value)
            ->orWhere('name',$value)
            ->orWhere('full_name',$value)
            ->pluck('currency_code');
    }

    public static function getCurrencySymbol($value)
    {
        return self::where('iso_3166_2',$value)
            ->orWhere('iso_3166_3',$value)
            ->orWhere('name',$value)
            ->orWhere('full_name',$value)
            ->get('currency_symbol')
            ->pluck('currency_symbol')->first();
    }


    public static function getDetails($value)
    {
        return self::where('iso_3166_2',$value)
            ->orWhere('iso_3166_3',$value)
            ->orWhere('name',$value)
            ->orWhere('full_name',$value)
            ->orWhere('capital',$value)
            ->orWhere('citizenship',$value)
            ->orWhere('country_code',$value)
            ->get();
    }
}
