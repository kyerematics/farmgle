<?php

namespace Farmgle\Modules;

use Farmgle\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Farmgle\Modules\Follower
 *
 * @property int $id
 * @property int $user_id
 * @property string $followable_type
 * @property int $followable_id
 * @property int $blocked
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $followable
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Follower newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Follower newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Follower query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Follower whereBlocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Follower whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Follower whereFollowableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Follower whereFollowableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Follower whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Follower whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Follower whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Follower extends Model
{




    protected $fillable =[
        'user_id','following','blocked'
    ];


    protected $attributes = [
        'blocked'   =>  false,
    ];



    /*
     * This Follower model belongs to the user
     * */
    public function followable()
    {
        return $this->morphTo();
    }
}
