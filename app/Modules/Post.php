<?php

namespace Farmgle\Modules;

use Farmgle\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Farmgle\Modules\Post
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Post whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $slug
 * @property int $strtotime
 * @property string $postable_type
 * @property int $postable_id
 * @property string $title
 * @property string $body
 * @property string $json
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $postable
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Post whereJson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Post wherePostableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Post wherePostableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Post whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Post whereStrtotime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Post whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Post whereTitle($value)
 */
class Post extends Model implements HasMedia
{
    use HasMediaTrait;


    protected $fillable = [
        'slug', 'title', 'body', 'json', 'strtotime'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function postable()
    {
        return $this->morphTo();
    }


    public function registerMediaCollections()
    {
        $this->addMediaCollection('post')
            ->registerMediaConversions(function (\Spatie\MediaLibrary\Models\Media $media){
                $this->addMediaConversion('listing')
                    ->width(577)
                    ->height(375);

                $this->addMediaConversion('cover_image')
                    ->width('832')
                    ->height('499');

                $this->addMediaConversion('tiles')
                    ->width(80)
                    ->height(80);
            });


    }
}
