<?php

namespace Farmgle\Modules;

use Illuminate\Database\Eloquent\Model;

/**
 * Farmgle\Modules\Service
 *
 * @property int $id
 * @property int $serviceable_id
 * @property string $serviceable_type
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $serviceable
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Service query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Service whereServiceableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Service whereServiceableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Service whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Service whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Service whereDescription($value)
 */
class Service extends Model
{

    protected $fillable = [
        'title',
        'description',
        /*'serviceable_id',
        'serviceable_type',*/

    ];


    public function serviceable()
    {
        return $this->morphTo();
    }
}
