<?php

namespace Farmgle;

use Farmgle\Modules\Follower;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * Farmgle\Logistics
 *
 * @property int $id
 * @property string|null $name
 * @property string $legal_name
 * @property string|null $email
 * @property string|null $phone
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $country
 * @property float|null $latitude
 * @property float|null $longitude
 * @property string|null $profile
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read \Farmgle\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereLegalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereProfile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Logistics whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Farmgle\Identity $identity
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Follower[] $follows
 */
class Logistics extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'legal_name',
        'email',
        'phone',
        'address',
        'city',
        'state',
        'country',
        'profile',
        'latitude',
        'longitude',
    ];



    /**
     * The attributes that should logistics' permission.
     *
     * @var array
     */
    protected $permissions = [
        'create-comment',
        'update-comment',
        'read-comment',
        'delete-comment',
        'read-product',
        'buy-product',
        'read-auction',
        'bid-auction',
    ];



    /*
     * Get the institution  belonging to the user
     * */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function identity()
    {
        return $this->morphOne(Identity::class, 'identifiable');
    }

    /*
     * This returns the models this user follows
     * */
    public function follows()
    {
        return $this->morphMany(Follower::class,'followable');
    }


    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('logistics')
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion('thumb-100')
                    ->width(100)
                    ->height(100);

                $this
                    ->addMediaConversion('thumb-200')
                    ->width(200)
                    ->height(200);
            });
    }
}
