<?php

namespace Farmgle;

use Farmgle\Modules\Follower;
use Farmgle\Modules\Post;
use Farmgle\Modules\Profile;
use Farmgle\Modules\Service;
use Farmgle\Modules\SocialMediaHandlers;
use Illuminate\Database\Eloquent\Model;
use Farmgle\Modules\Media as Association;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Traits\HasPermissions;

/**
 * Farmgle\Institution
 *
 * @property int $id
 * @property string $name
 * @property string|null $email
 * @property string|null $phone
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string|null $service_hours
 * @property string|null $certificate
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Farmgle\Modules\Profile $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Service[] $services
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\SocialMediaHandlers[] $socialMediaHandlers
 * @property-read \Farmgle\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereCertificate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereServiceHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereUserId($value)
 * @mixin \Eloquent
 * @property float|null $latitude
 * @property float|null $longitude
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Institution whereLongitude($value)
 * @property-read \Farmgle\Identity $identity
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Media[] $mediaAssociation
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Follower[] $follows
 */
class Institution extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'address',
        'city',
        'state',
        'country',
        'latitude',
        'longitude',
    ];



    /**
     * The attributes that should farmer's permission.
     *
     * @return array
     */
    protected function permissions(){
        return  [
            'create-blog',
            'update-blog',
            'delete-blog',
            'read-blog',
            'create-profile',
            'update-profile',
            'read-profile',
            'delete-profile',
            'create-comment',
            'update-comment',
            'read-comment',
            'delete-comment',
            'read-product',
            'buy-product',
            'read-auction',
            'bid-auction',
        ];
    }



    /*
     * Get the institution  belonging to the user
     * */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /*Institutions has many social media handlers
   * Get all institutions social media accounts
   */
    public function socialMediaHandlers(){
        return $this->morphMany(SocialMediaHandlers::class, 'sociable');
    }


    public function profile()
    {
        return $this->morphOne(Profile::class, 'profileable');
    }

    public function services()
    {

        return $this->morphMany(Service::class, 'serviceable');
    }

    public function identity()
    {
        return $this->morphOne(Identity::class, 'identifiable');
    }

    /*
     * This returns the models this user follows
     * */
    public function follows()
    {
        return $this->morphMany(Follower::class,'followable');
    }

    /*
     * This method is responsible for storing all media for safe keep
     * */
    public function mediaAssociation()
    {
        return $this->morphMany(Association::class, 'associable');
    }


    public function posters()
    {
        return $this->morphMany(Post::class,'postable');
    }


    public function registerMediaCollections()
    {

        $this
            ->addMediaCollection('listing')
            ->registerMediaConversions(function (Media $media = null){
                $this
                    ->addMediaConversion('thumb-1000')
                    ->width(1000)
                    ->height('650');

                $this
                    ->addMediaConversion('thumb-577')
                    ->width(577)
                    ->height(375);

                $this
                    ->addMediaConversion('thumb-258')
                    ->width(258)
                    ->height(167);

                $this
                    ->addMediaConversion('thumb-100')
                    ->width(100)
                    ->height(100);

                $this
                    ->addMediaConversion('thumb-200')
                    ->width(200)
                    ->height(200);
            });

        $this
            ->addMediaCollection('cover-page')
            ->registerMediaConversions(function (Media $media = null){
                $this
                    ->addMediaConversion('portrait')
                    ->width(592)
                    ->height(437);

                $this
                    ->addMediaConversion('landscape')
                    ->width(365)
                    ->height(590);

                $this
                    ->addMediaConversion('small')
                    ->width(278)
                    ->height(299);
            });

        /*
         * This image collection is for singled out images
         *
         * */
        $this
            ->addMediaCollection('services')
            ->registerMediaConversions(function (Media $media = null){
                $this
                    ->addMediaConversion('why_us')
                    ->width(396)
                    ->height(396);

                $this
                    ->addMediaConversion('our_profile')
                    ->width(650)
                    ->height(700);

                $this
                    ->addMediaConversion('mission')
                    ->width(874)
                    ->height(603);

                $this
                    ->addMediaConversion('appointment')
                    ->width(710)
                    ->height(644);
            });

    }



}
