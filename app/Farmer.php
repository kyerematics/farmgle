<?php

namespace Farmgle;

use Farmgle\Modules\Follower;
use Farmgle\Modules\Product;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Farmgle\Modules\SocialMediaHandlers;
use Farmgle\Modules\Media as Association;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Farmgle\Farmer
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\SocialMediaHandlers[] $socialMediaHandlers
 * @property-read \Farmgle\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $legal_name
 * @property string|null $email
 * @property string|null $phone
 * @property string $city
 * @property string $state
 * @property string $country
 * @property float|null $latitude
 * @property float|null $longitude
 * @property string|null $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereLegalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereProfile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Farmer whereState($value)
 * @property-read \Farmgle\Identity $identity
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Media[] $mediaAssociation
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Follower[] $follows
 */
class Farmer extends Model implements HasMedia
{
    use HasMediaTrait;


    protected $fillable = [
        'legal_name',
        'email',
        'phone',
        'address',
        'city',
        'state',
        'country',
        'profile',
        'latitude',
        'longitude',
    ];


    /**
     * The attributes that should farmer's permission.
     *
     * @var array
     */
    protected $permissions = [
        'read-blog',
        'read-profile',
        'create-appointment',
        'update-appointment',
        'read-appointment',
        'delete-appointment',
        'create-comment',
        'update-comment',
        'read-comment',
        'delete-comment',
        'create-product',
        'update-product',
        'read-product',
        'delete-product',
        'buy-product',
        'sell-product',
        'auction-service',
        'auction-product',
        'create-auction',
        'update-auction',
        'delete-auction',
        'read-auction',
        'bid-auction'
    ];

    /*
     * Get the institution  belonging to the user
     * */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /*Freelancer has many social media handlers
    * Get all freelancer's social media accounts
    */
    public function socialMediaHandlers(){
        return $this->morphMany(SocialMediaHandlers::class, 'sociable');
    }


    public function products()
    {
        return $this->morphMany(Product::class,'producible');
    }


    /*
     * This method is responsible for storing all media for safe keep
     * */
    public function mediaAssociation()
    {
        return $this->morphMany(Association::class, 'associable');
    }


    /*
     * A farmer portfolio has one identity
     *
     * */
    public function identity()
    {
        return $this->morphOne(Identity::class, 'identifiable');
    }


    /*
     * This returns the models this user follows
     * */
    public function follows()
    {
        return $this->morphMany(Follower::class,'followable');
    }


    public function registerMediaCollections()
    {

        $this
            ->addMediaCollection('listing')
            ->registerMediaConversions(function (Media $media = null){
                $this
                    ->addMediaConversion('thumb-1000')
                    ->width(1000)
                    ->height('650');

                $this
                    ->addMediaConversion('thumb-577')
                    ->width(577)
                    ->height(375);

                $this
                    ->addMediaConversion('thumb-258')
                    ->width(258)
                    ->height(167);

                $this
                    ->addMediaConversion('thumb-100')
                    ->width(100)
                    ->height(100);

                $this
                    ->addMediaConversion('thumb-200')
                    ->width(200)
                    ->height(200);
            });


        /*
         * Values here are based on guess work
         * Values needs to be recalculated
         * */
        $this
            ->addMediaCollection('slider')
            ->registerMediaConversions(function (Media $media){
                $this
                    ->addMediaConversion('slider')
                    ->width(577)
                    ->height(375);
            });

    }
}
