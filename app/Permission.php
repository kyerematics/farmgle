<?php

namespace Farmgle;

use Spatie\Permission\Models\Permission as SpatiePermission;

/**
 * Farmgle\Permission
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Permission permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Permission role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Permission whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Permission whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Permission extends SpatiePermission
{
    public static function defaultPermissions()
    {
        return [
            'create-blog',
            'update-blog',
            'delete-blog',
            'read-blog',
            'create-profile',
            'update-profile',
            'read-profile',
            'delete-profile',
            'create-appointment',
            'update-appointment',
            'read-appointment',
            'delete-appointment',
            'create-comment',
            'update-comment',
            'read-comment',
            'delete-comment',
            'create-product',
            'update-product',
            'read-product',
            'delete-product',
            'buy-product',
            'sell-product'
        ];
    }
}
