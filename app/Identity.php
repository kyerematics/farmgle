<?php

namespace Farmgle;

use Illuminate\Database\Eloquent\Model;

/**
 * Farmgle\Identity
 *
 * @property int $id
 * @property string $identifiable_type
 * @property int $identifiable_id
 * @property string $identifier
 * @property string|null $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $identifiable
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Identity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Identity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Identity query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Identity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Identity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Identity whereIdentifiableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Identity whereIdentifiableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Identity whereIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Identity whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Identity whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Identity extends Model
{
    protected $fillable = [
        'identifier','slug'
    ];


    public function identifiable()
    {
        return $this->morphTo();
    }
}
