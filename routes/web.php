<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (app()->environment('local')){

    /*
     * Testing Routes*/
    Route::get('/test', function (){
        return \Illuminate\Support\Facades\Cache::forget('GHS/USD');
    });
    Route::get('/converter','ImageConverterController@index');
    Route::post('/converter','ImageConverterController@store')->name('image.converter');

    /*End of Testing routes*/
}




Auth::routes(['verify' => true ]);

Route::redirect('/register','/login');
Route::post('register','Auth\RegisterController@register')->name('register');






Route::middleware([
    'auth',
    'verified',
    \Farmgle\Http\Middleware\NotRegistered::class,
    ])->group(function (){


    Route::get('/','TimelineController@index')->name('timeline');
    Route::get('/user', 'HomeController@index')->name('home');
    /** Registration routes**/

    Route::namespace('Registration')->group(function (){
        Route::get('register/criterion', 'CriterionController@index')->name('selectCriterion');
        Route::post('register/criterion', 'CriterionController@store')->name('criterion');

//Route::get('wizard/user/{step?}', 'UserWizardController@wizard')->name('wizard.user');
//Route::post('wizard/user/{step}', 'UserWizardController@wizardPost')->name('wizard.user.post');
        /*Freelancer's Registration*/
        Route::get('registration/serviceOriented/{step?}','ServiceOrientedController@wizard')->name('wizard.serviceOriented');
        Route::post('registration/serviceOriented/{step}','ServiceOrientedController@wizardPost')->name('wizard.serviceOriented.post');

        Route::get('registration/seller/{step?}','SellerController@wizard' )-> name('wizard.seller');
        Route::post('registration/seller/{step}','SellerController@wizardPost' )-> name('wizard.seller.post');



        Route::get('registration/logistics/{step?}','LogisticsController@wizard' )-> name('wizard.logistics');
        Route::post('registration/logistics/{step}','LogisticsController@wizardPost' )-> name('wizard.logistics.post');
    });

    /**
     * General Pages
     *
     * */
    Route::get('posts', 'PostController@index')->name('allPosts');
    Route::get('services','ServicesOriented\ServiceOrientedController@index')->name('allServices');

    Route::get('posts/{slug?}', 'PostController@show')->name('post');

/*
 * Follower/Following routes
 *
 * */
    Route::post('follow/{identifier}','FollowerController@store')->name('follow');
    Route::delete('unfollow/{identifier}', 'FollowerController@destroy')->name('unfollow');

});

/**
 * Seller's routes
 * */
Route::namespace('Seller')->group(function (){

    Route::get('market','SellerController@index')->name('allProducts');
    Route::get('market/shop/{seller}', 'SellerController@shop')->name('sellerShop');
    Route::get('market/shop/{seller}/{product?}','SellerController@showProduct')->name('singleProduct');
    //Route::get('market/{farmer}','FarmerController@show')->name('farmer');


    /*
     * Cart Routes
     * */

});



/**
 * Dynamic Routes
 *  Service Oriented Routes
 * */

Route::middleware(['auth','verified'])->group(function (){
    Route::namespace('ServicesOriented')->group(function (){

        Route::get('{services?}','ServiceOrientedController@show')->name('services');
    });


});

