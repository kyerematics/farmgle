<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('producible');
            /*$table->bigInteger('producible_id');
            $table->string('producible_type');*/
            $table->string('product');
            $table->uuid('uuid');
            $table->text('description');
            $table->decimal('price',20,2);
            $table->string('currency');
            $table->bigInteger('quantity');
            $table->string('unit_of_measurement')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
